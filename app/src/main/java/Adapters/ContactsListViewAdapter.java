package Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.hello.Contact;
import com.example.hello.R;

import java.util.ArrayList;

import static java.lang.Math.min;

public class ContactsListViewAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Contact> contacts;

    public ContactsListViewAdapter(Context context, ArrayList<Contact> contacts) {
        this.context = context;
        this.contacts = contacts;
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Object getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.contacts_list_view_cell, parent, false);
        }

        convertView.setSelected(true);
        setViewAppearanceAccordingContact((ViewGroup) convertView, contacts.get(position));

        return convertView;
    }

    private void setViewAppearanceAccordingContact(ViewGroup view, Contact contact) {
        TextView nameLettersView = view.findViewById(R.id.nameLettersView);
        TextView nameTextView = view.findViewById(R.id.contactNameTextView);
        TextView numberTextView = view.findViewById(R.id.contactNumberTextView);

        numberTextView.setText(contact.getName());
        nameTextView.setText(contact.getNumber());

        String[] nameParts = contact.getName().trim().split(" ");
        nameLettersView.setText("");
        for(int i = 0; i < min(2, nameParts.length); ++i) {
            nameLettersView.append(nameParts[i].substring(0,1));
        }
    }
}
