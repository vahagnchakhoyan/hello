package Managers;

import android.app.Activity;

import com.example.hello.Contact;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static Managers.RequestsManager.freeRequestCode;
import static Managers.RequestsManager.noNeedToSendRequestCode;
import static Managers.RequestsManager.requestRejectionCode;
import static com.example.hello.Properties.API_deleteContact;
import static com.example.hello.Properties.API_editContact;
import static com.example.hello.Properties.API_getContacts;
import static com.example.hello.Properties.API_registerContact;
import static com.example.hello.Properties.serverBase;

public final class ContactsManager {
    private static OkHttpClient client = new OkHttpClient();

    public static abstract class ContactsRequestBase extends RequestsManager.RequestBase {
        protected boolean justWaitingForAnswerWithoutCompletion;

        protected abstract void correctContactsAccordingToResult(HashMap<String, Object> requestResult);

        @Override
        protected void resultIsReady(int requestNumber, HashMap<String, Object> requestResult) {
            correctContactsAccordingToResult(requestResult);

            if(justWaitingForAnswerWithoutCompletion) {
                justWaitingForAnswerWithoutCompletion = false;
                inProcessRequestNumber = freeRequestCode;
                return;
            }

            if (requestNumber != inProcessRequestNumber) {
                return;
            }

            if (mustWaitForNewCompletion) {
                setUnprocessedResult(inProcessRequestNumber, requestResult);
            } else {
                getCompletion().execute(requestResult);
                executedCompletion(requestNumber);
            }

            inProcessRequestNumber = freeRequestCode;
        }

        @Override
        public void applyNewCompletion(RequestsManager.Completion newCompletion, int requestNumber) {
            if (inProcessRequestNumber == freeRequestCode || inProcessRequestNumber != requestNumber) {
                newCompletion.execute(getUnprocessedResult(requestNumber));
            } else {
                mustWaitForNewCompletion = false;
                completion = newCompletion;
            }
        }
    }

    private ContactsManager() {}

    private static int requestsNumber = 0;
    private static HashMap<String, HashMap<String, Object>> unprocessedResults = new HashMap<>();
    private static void setUnprocessedResult(int requestNumber, HashMap<String, Object> requestResult) {
        if (requestNumber != freeRequestCode) { unprocessedResults.put(Integer.toString(requestNumber), requestResult); }
    }
    private static HashMap<String, Object> getUnprocessedResult(int requestNumber) {
        return unprocessedResults.remove(Integer.toString(requestNumber));
    }

    private static ArrayList<Contact> contacts;

    public static Contact getContactByNumber(final String number) {
        for(int counter = 0; counter < contacts.size(); ++counter) {
            if(contacts.get(counter).getNumber().equals(number)) {
                return  contacts.get(counter);
            }
        }

        return null;
    }
    public static Contact getContactByPosition(int position) {
        return contacts.get(position);
    }
    public static int getIndexOfContact(Contact contact) {
        return contacts.indexOf(contact);
    }

    private static void contactDeleted(Contact contact) {
        contacts.remove(contact);
    }
    private static void registeredContact(Contact contact) {
        contacts.add(contact);
    }

    public static void loggingOut() {
        contacts = null;
    }



    public static class GetContacts extends ContactsRequestBase {
        private GetContacts() {
            inProcessRequestNumber = freeRequestCode;
            mustWaitForNewCompletion = false;
        }

        private Call call;

        public int sendRequest(final Activity activity, String number, String token, RequestsManager.Completion completion) {
            if(justWaitingForAnswerWithoutCompletion) {
                this.completion = completion;
                mustWaitForNewCompletion = false;
                justWaitingForAnswerWithoutCompletion = false;
                return inProcessRequestNumber;
            }

            if(inProcessRequestNumber != freeRequestCode) {
                return requestRejectionCode;
            }

            if(contacts != null) {
                HashMap<String, Object> requestResult = new HashMap<>();
                requestResult.put(isSuccessKey, true);
                requestResult.put(contactsKey, contacts);
                completion.execute(requestResult);
                return noNeedToSendRequestCode;
            }

            RequestBody formBody = new FormBody.Builder().add("number", number).add("token", token).build();
            final Request request = new Request.Builder().url(serverBase + API_getContacts).post(formBody).build();
            call = client.newCall(request);

            final int currentRequestNumber = ++requestsNumber;
            this.completion = completion;
            mustWaitForNewCompletion = false;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final HashMap<String, Object> requestResult = new HashMap<>();
                    try {
                        final Response response = call.execute();
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    try {
                                        JSONObject reader = new JSONObject(response.body().string());
                                        JSONArray contactsJsonArray = (JSONArray) reader.get("contacts");
                                        ArrayList<Contact> contacts = new ArrayList<Contact>();
                                        for(int counter = 0; counter < contactsJsonArray.length(); ++counter) {
                                            try {contacts.add(new Contact(contactsJsonArray.getJSONObject(counter)));} catch(Exception e) {}
                                        }
                                        requestResult.put(contactsKey, contacts);
                                        requestResult.put(isSuccessKey, true);
                                    } catch (Exception e) {
                                        requestResult.put(isSuccessKey, false);
                                    } finally {
                                        response.body().close();
                                    }
                                } else {
                                    requestResult.put(isSuccessKey, false);
                                }

                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    } catch(IOException e) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                requestResult.put(isSuccessKey, false);
                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    }
                }
            }).start();

            inProcessRequestNumber = currentRequestNumber;
            return currentRequestNumber;
        }

        @Override
        protected void correctContactsAccordingToResult(HashMap<String, Object> requestResult) {
            if((boolean) requestResult.get(isSuccessKey)) {
                contacts = (ArrayList<Contact>) requestResult.get(contactsKey);
            }
        }

        public void killRequest(int requestCode) {
            if (requestCode != inProcessRequestNumber) {
                getUnprocessedResult(requestCode);
                return;
            }

            mustWaitForNewCompletion = false;
            justWaitingForAnswerWithoutCompletion = true;
        }
    }
    public static GetContacts getContacts = new GetContacts();



    public static class DeleteContact extends ContactsRequestBase {
        private DeleteContact() {
            inProcessRequestNumber = freeRequestCode;
            mustWaitForNewCompletion = false;
        }

        private Call call;

        public int sendRequest(final Activity activity, String number, String token, final Contact contact, RequestsManager.Completion completion) {
            if(justWaitingForAnswerWithoutCompletion) {
                this.completion = completion;
                mustWaitForNewCompletion = false;
                justWaitingForAnswerWithoutCompletion = false;
                return inProcessRequestNumber;
            }

            if (inProcessRequestNumber != freeRequestCode) {
                return requestRejectionCode;
            }

            RequestBody formBody = new FormBody.Builder().add("number", number).add("token", token).add("contactNumber", contact.getNumber()).build();
            final Request request = new Request.Builder().url(serverBase + API_deleteContact).post(formBody).build();
            call = client.newCall(request);

            final int currentRequestNumber = ++requestsNumber;
            this.completion = completion;
            mustWaitForNewCompletion = false;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final HashMap<String, Object> requestResult = new HashMap<>();
                    try {
                        final Response response = call.execute();
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    try {
                                        JSONObject reader = new JSONObject(response.body().string());
                                        requestResult.put(deleteContactSuccessKey, reader.get("success"));
                                        requestResult.put(deletedContactKey, contact);
                                        requestResult.put(isSuccessKey, true);
                                    } catch (Exception e) {
                                        requestResult.put(isSuccessKey, false);
                                    } finally {
                                        response.body().close();
                                    }
                                } else {
                                    requestResult.put(isSuccessKey, false);
                                }

                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    } catch(IOException e) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                requestResult.put(isSuccessKey, false);
                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    }
                }
            }).start();

            inProcessRequestNumber = currentRequestNumber;
            return currentRequestNumber;
        }

        @Override
        protected void correctContactsAccordingToResult(HashMap<String, Object> requestResult) {
            if((boolean) requestResult.get(isSuccessKey)) {
                if((boolean) requestResult.get(isSuccessKey) && (boolean) requestResult.get(deleteContactSuccessKey)) {
                    contactDeleted((Contact) requestResult.get(deletedContactKey));
                }
            }
        }

        public void killRequest(int requestCode) {
            if (requestCode != inProcessRequestNumber) {
                getUnprocessedResult(requestCode);
                return;
            }

            mustWaitForNewCompletion = false;
            justWaitingForAnswerWithoutCompletion = true;
        }
    }
    public static DeleteContact deleteContact = new DeleteContact();




    public static class EditContact extends ContactsRequestBase {
        private EditContact() {
            inProcessRequestNumber = freeRequestCode;
            mustWaitForNewCompletion = false;
        }

        private Call call;

        public int sendRequest(final Activity activity, String number, String token, final Contact oldContact, final Contact editedContact, RequestsManager.Completion completion) {
            if(justWaitingForAnswerWithoutCompletion) {
                this.completion = completion;
                mustWaitForNewCompletion = false;
                justWaitingForAnswerWithoutCompletion = false;
                return inProcessRequestNumber;
            }

            if (inProcessRequestNumber != freeRequestCode) {
                return requestRejectionCode;
            }

            RequestBody formBody = new FormBody.Builder().add("number", number).add("token", token).add("contactNumber", editedContact.getNumber()).add("contactName", editedContact.getName()).build();
            final Request request = new Request.Builder().url(serverBase + API_editContact).post(formBody).build();
            call = client.newCall(request);

            final int currentRequestNumber = ++requestsNumber;
            this.completion = completion;
            mustWaitForNewCompletion = false;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final HashMap<String, Object> requestResult = new HashMap<>();
                    try {
                        final Response response = call.execute();
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    try {
                                        JSONObject reader = new JSONObject(response.body().string());
                                        requestResult.put(editContactSuccessKey, reader.get("success"));
                                        requestResult.put(editedContactKey, editedContact);
                                        requestResult.put(oldContactKey, oldContact);
                                        requestResult.put(isSuccessKey, true);
                                    } catch (Exception e) {
                                        requestResult.put(isSuccessKey, false);
                                    } finally {
                                        response.body().close();
                                    }
                                } else {
                                    requestResult.put(isSuccessKey, false);
                                }

                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    } catch(IOException e) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                requestResult.put(isSuccessKey, false);
                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    }
                }
            }).start();

            inProcessRequestNumber = currentRequestNumber;
            return currentRequestNumber;
        }

        @Override
        protected void correctContactsAccordingToResult(HashMap<String, Object> requestResult) {
            if((boolean) requestResult.get(isSuccessKey) && (boolean) requestResult.get(editContactSuccessKey)) {
                ((Contact) requestResult.get(oldContactKey)).setFromAntother((Contact) requestResult.get(editedContactKey));
            }
        }

        public void killRequest(int requestCode) {
            if (requestCode != inProcessRequestNumber) {
                getUnprocessedResult(requestCode);
                return;
            }

            mustWaitForNewCompletion = false;
            justWaitingForAnswerWithoutCompletion = true;
        }
    }
    public static EditContact editContact = new EditContact();




    public static class RegisterContact extends ContactsRequestBase {
        private RegisterContact() {
            inProcessRequestNumber = freeRequestCode;
            mustWaitForNewCompletion = false;
        }

        private Call call;

        public int sendRequest(final Activity activity, String number, String token, final String contactNumber, final String contactName, RequestsManager.Completion completion) {
            if(justWaitingForAnswerWithoutCompletion) {
                this.completion = completion;
                mustWaitForNewCompletion = false;
                justWaitingForAnswerWithoutCompletion = false;
                return inProcessRequestNumber;
            }

            if (inProcessRequestNumber != freeRequestCode) {
                return requestRejectionCode;
            }

            final Contact newContact = new Contact(contactNumber, contactName);

            RequestBody formBody = new FormBody.Builder().add("number", number).add("token", token).add("contactNumber", newContact.getNumber()).add("contactName", newContact.getName()).build();
            final Request request = new Request.Builder().url(serverBase + API_registerContact).post(formBody).build();
            call = client.newCall(request);

            final int currentRequestNumber = ++requestsNumber;
            this.completion = completion;
            mustWaitForNewCompletion = false;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final HashMap<String, Object> requestResult = new HashMap<>();
                    try {
                        final Response response = call.execute();
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    try {
                                        JSONObject reader = new JSONObject(response.body().string());
                                        requestResult.put(isRegisterSuccessKey, reader.get("success"));
                                        requestResult.put(newContactKey, newContact);
                                        requestResult.put(isSuccessKey, true);
                                    } catch (Exception e) {
                                        requestResult.put(isSuccessKey, false);
                                    } finally {
                                        response.body().close();
                                    }
                                } else {
                                    requestResult.put(isSuccessKey, false);
                                }

                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    } catch(IOException e) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                requestResult.put(isSuccessKey, false);
                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    }
                }
            }).start();

            inProcessRequestNumber = currentRequestNumber;
            return currentRequestNumber;
        }

        @Override
        protected void correctContactsAccordingToResult(HashMap<String, Object> requestResult) {
            if((boolean) requestResult.get(isSuccessKey) && (boolean) requestResult.get(isRegisterSuccessKey)) {
                registeredContact((Contact) requestResult.get(newContactKey));
            }
        }

        public void killRequest(int requestCode) {
            if (requestCode != inProcessRequestNumber) {
                getUnprocessedResult(requestCode);
                return;
            }

            mustWaitForNewCompletion = false;
            justWaitingForAnswerWithoutCompletion = true;
        }
    }
    public static RegisterContact registerContact = new RegisterContact();
}
