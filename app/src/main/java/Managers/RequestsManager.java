package Managers;

import android.app.Activity;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.example.hello.Properties.API_authenticateWithToken;
import static com.example.hello.Properties.API_confirmPin;
import static com.example.hello.Properties.API_isNumberRegistered;
import static com.example.hello.Properties.API_logIn;
import static com.example.hello.Properties.API_logOut;
import static com.example.hello.Properties.API_numberAvailabilityCheck;
import static com.example.hello.Properties.serverBase;

public final class RequestsManager {
    private static OkHttpClient client = new OkHttpClient();

    public static abstract class RequestBase {
        protected Completion completion;
        protected int inProcessRequestNumber;
        protected boolean mustWaitForNewCompletion;

        public static final String isSuccessKey = "isSuccess";
        public static final String checkResultKey = "checkResultKey";
        public static final String numberKey = "number";
        public static final String authenticationSuccessKey = "isAuthenticationSuccess";
        public static final String confirmationSuccessKey = "isConfirmationSuccess";
        public static final String logInSuccessKey = "isLogInSuccess";
        public static final String tokenKey = "token";
        public static final String contactsKey = "contacts";
        public static final String deleteContactSuccessKey = "deleteContactSuccess";
        public static final String deletedContactKey = "deletedContact";
        public static final String editContactSuccessKey = "editContactSuccess";
        public static final String editedContactKey = "editedContact";
        public static final String oldContactKey = "oldContact";
        public static final String newContactKey = "newContact";
        public static final String isRegisteredKey = "isRegistered";
        public static final String isRegisterSuccessKey = "isRegisterSuccess";

        Completion getCompletion() {
            return completion;
        }

        public boolean waitForNewCompletion(int requestNumber) {
            if (requestNumber != inProcessRequestNumber) {
                return false;
            }

            completion = null;
            mustWaitForNewCompletion = true;
            return true;
        }

        public void executedCompletion(int requestNumber) {
            if (requestNumber != inProcessRequestNumber) {
                return;
            }

            completion = null;
        }

        public void applyNewCompletion(Completion newCompletion, int requestNumber) {
            if (inProcessRequestNumber == freeRequestCode || inProcessRequestNumber != requestNumber) {
                newCompletion.execute(getUnprocessedResult(requestNumber));
            } else {
                mustWaitForNewCompletion = false;
                completion = newCompletion;
            }
        }

        protected void resultIsReady(int requestNumber, HashMap<String, Object> requestResult) {
                    if (requestNumber != inProcessRequestNumber) {
                        return;
                    }

            if (mustWaitForNewCompletion) {
                setUnprocessedResult(inProcessRequestNumber, requestResult);
            } else {
                getCompletion().execute(requestResult);
                executedCompletion(requestNumber);
            }

            inProcessRequestNumber = freeRequestCode;
        }
    }

    public interface Completion {
        void execute(HashMap<String, Object> result);
    }

    private RequestsManager() {}

    public static final int requestRejectionCode = 0;
    public static final int freeRequestCode = 0;
    public static final int noNeedToSendRequestCode = -1;

    private static HashMap<String, HashMap<String, Object>> unprocessedResults = new HashMap<>();
    private static void setUnprocessedResult(int requestNumber, HashMap<String, Object> requestResult) {
        if (requestNumber != freeRequestCode) { unprocessedResults.put(Integer.toString(requestNumber), requestResult); }
    }
    private static HashMap<String, Object> getUnprocessedResult(int requestNumber) {
        return unprocessedResults.remove(Integer.toString(requestNumber));
    }

    private static int requestsNumber = 0;



    public static class NumberAvailabilityCheck extends RequestBase {
        private NumberAvailabilityCheck() {
            inProcessRequestNumber = freeRequestCode;
            mustWaitForNewCompletion = false;
        }

        private Call call;

        public int sendRequest(final Activity activity, final String number, Completion completion) {
            if (inProcessRequestNumber != freeRequestCode) {
                return requestRejectionCode;
            }

            RequestBody formBody = new FormBody.Builder().add("number", number).build();
            Request request = new Request.Builder().url(serverBase + API_numberAvailabilityCheck).post(formBody).build();
            call = client.newCall(request);

            final int currentRequestNumber = ++requestsNumber;
            this.completion = completion;
            mustWaitForNewCompletion = false;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final HashMap<String, Object> requestResult = new HashMap<>();
                    try {
                        final Response response = call.execute();
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(response.isSuccessful()) {
                                    try {
                                        JSONObject reader = new JSONObject(response.body().string());
                                        requestResult.put(checkResultKey, reader.getBoolean("result"));
                                        requestResult.put(numberKey, number);
                                        requestResult.put(isSuccessKey, true);
                                    } catch (Exception e) {
                                        requestResult.put(isSuccessKey, false);
                                    } finally {
                                        response.body().close();
                                    }
                                } else {
                                    requestResult.put(isSuccessKey, false);
                                }

                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    } catch (IOException e) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                requestResult.put(isSuccessKey, false);
                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    }
                }
            }).start();

            inProcessRequestNumber = currentRequestNumber;
            return currentRequestNumber;
        }

        public void killRequest(int requestCode) {
            if (requestCode != inProcessRequestNumber) {
                getUnprocessedResult(requestCode);
                return;
            }

            call.cancel();
            mustWaitForNewCompletion = false;
            inProcessRequestNumber = freeRequestCode;
        }
    }
    public static NumberAvailabilityCheck numberAvailabilityCheck = new NumberAvailabilityCheck();



    public static class AuthenticateWithToken extends RequestBase {
        private AuthenticateWithToken() {
            inProcessRequestNumber = freeRequestCode;
            mustWaitForNewCompletion = false;
        }

        private Call call;

        public int sendRequest(final Activity activity, final String token, final String number, Completion completion) {
            if (inProcessRequestNumber != freeRequestCode) {
                return requestRejectionCode;
            }

            RequestBody formBody = new FormBody.Builder().add("number", number).add("token", token).build();
            Request request = new Request.Builder().url(serverBase + API_authenticateWithToken).post(formBody).build();
            call = client.newCall(request);

            final int currentRequestNumber = ++requestsNumber;
            this.completion = completion;
            mustWaitForNewCompletion = false;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final HashMap<String, Object> requestResult = new HashMap<>();
                    try {
                        final Response response = call.execute();
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    try {
                                        JSONObject reader = new JSONObject(response.body().string());
                                        requestResult.put(authenticationSuccessKey, reader.getBoolean("success"));
                                        requestResult.put(tokenKey, token);
                                        requestResult.put(numberKey, number);
                                        requestResult.put(isSuccessKey, true);
                                    } catch (Exception e) {
                                        requestResult.put(isSuccessKey, false);
                                    } finally {
                                        response.body().close();
                                    }
                                } else {
                                    requestResult.put(isSuccessKey, false);
                                }

                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    } catch (IOException e) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                requestResult.put(isSuccessKey, false);
                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    }
                }
            }).start();

            inProcessRequestNumber = currentRequestNumber;
            return currentRequestNumber;
        }

        public void killRequest(int requestCode) {
            if (requestCode != inProcessRequestNumber) {
                getUnprocessedResult(requestCode);
                return;
            }

            call.cancel();
            mustWaitForNewCompletion = false;
            inProcessRequestNumber = freeRequestCode;
        }
    }
    public static AuthenticateWithToken authenticateWithToken = new AuthenticateWithToken();



    public static class ConfirmPin extends RequestBase {
        private ConfirmPin() {
            inProcessRequestNumber = freeRequestCode;
            mustWaitForNewCompletion = false;
        }

        private Call call;

        public int sendRequest(final Activity activity, final String number, final String pin, Completion completion) {
            if (inProcessRequestNumber != freeRequestCode) {
                return requestRejectionCode;
            }

            RequestBody formBody = new FormBody.Builder().add("number", number).add("pin", pin).build();
            Request request = new Request.Builder().url(serverBase + API_confirmPin).post(formBody).build();
            call = client.newCall(request);

            final int currentRequestNumber = ++requestsNumber;
            this.completion = completion;
            mustWaitForNewCompletion = false;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final HashMap<String, Object> requestResult = new HashMap<>();
                    try {
                        final Response response = call.execute();
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    try {
                                        JSONObject reader = new JSONObject(response.body().string());
                                        requestResult.put(confirmationSuccessKey, reader.getBoolean("success"));
                                        requestResult.put(tokenKey, reader.getString("token"));
                                        requestResult.put(numberKey, number);
                                        requestResult.put(isSuccessKey, true);
                                    } catch (Exception e) {
                                        requestResult.put(isSuccessKey, false);
                                    } finally {
                                        response.body().close();
                                    }
                                } else {
                                    requestResult.put(isSuccessKey, false);
                                }

                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    } catch (IOException e) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                requestResult.put(isSuccessKey, false);
                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    }
                }
            }).start();

            inProcessRequestNumber = currentRequestNumber;
            return currentRequestNumber;
        }

        public void killRequest(int requestCode) {
            if (requestCode != inProcessRequestNumber) {
                getUnprocessedResult(requestCode);
                return;
            }

            call.cancel();
            mustWaitForNewCompletion = false;
            inProcessRequestNumber = freeRequestCode;
        }
    }
    public static ConfirmPin confirmPin = new ConfirmPin();



    public static class LogIn extends RequestBase {
        private LogIn() {
            inProcessRequestNumber = freeRequestCode;
            mustWaitForNewCompletion = false;
        }

        private Call call;

        public int sendRequest(final Activity activity, final String number, final String pin, Completion completion) {
            if (inProcessRequestNumber != freeRequestCode) {
                return requestRejectionCode;
            }

            RequestBody formBody = new FormBody.Builder().add("number", number).add("pin", pin).build();
            Request request = new Request.Builder().url(serverBase + API_logIn).post(formBody).build();
            call = client.newCall(request);

            final int currentRequestNumber = ++requestsNumber;
            this.completion = completion;
            mustWaitForNewCompletion = false;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final HashMap<String, Object> requestResult = new HashMap<>();
                    try {
                        final Response response = call.execute();
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    try {
                                        JSONObject reader = new JSONObject(response.body().string());
                                        requestResult.put(logInSuccessKey, reader.getBoolean("success"));
                                        requestResult.put(tokenKey, reader.getString("token"));
                                        requestResult.put(numberKey, number);
                                        requestResult.put(isSuccessKey, true);
                                    } catch (Exception e) {
                                        requestResult.put(isSuccessKey, false);
                                    } finally {
                                        response.body().close();
                                    }
                                } else {
                                    requestResult.put(isSuccessKey, false);
                                }

                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    } catch (IOException e) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                requestResult.put(isSuccessKey, false);
                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    }
                }
            }).start();

            inProcessRequestNumber = currentRequestNumber;
            return currentRequestNumber;
        }

        public void killRequest(int requestCode) {
            if (requestCode != inProcessRequestNumber) {
                getUnprocessedResult(requestCode);
                return;
            }

            call.cancel();
            mustWaitForNewCompletion = false;
            inProcessRequestNumber = freeRequestCode;
        }
    }
    public static LogIn logIn = new LogIn();



    public static class LogOut extends RequestBase {
        private LogOut() {
            inProcessRequestNumber = freeRequestCode;
            mustWaitForNewCompletion = false;
        }

        private Call call;

        public int sendRequest(Activity activity, String number, String token) {
            if (inProcessRequestNumber != freeRequestCode) {
                return requestRejectionCode;
            }

            RequestBody formBody = new FormBody.Builder().add("number", number).add("token", token).build();
            Request request = new Request.Builder().url(serverBase + API_logOut).post(formBody).build();
            call = client.newCall(request);

            final int currentRequestNumber = ++requestsNumber;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Response response = call.execute();
                        if(response.isSuccessful()) response.body().close();
                    } catch (IOException e) {}

                    if (currentRequestNumber != inProcessRequestNumber) {
                        return;
                    }

                    inProcessRequestNumber = freeRequestCode;
                }
            }).start();

            inProcessRequestNumber = currentRequestNumber;
            return currentRequestNumber;
        }

        public void killRequest(int requestCode) {
            if (requestCode != inProcessRequestNumber) {
                getUnprocessedResult(requestCode);
                return;
            }

            call.cancel();
            mustWaitForNewCompletion = false;
            inProcessRequestNumber = freeRequestCode;
        }
    }
    public static LogOut logOut = new LogOut();



    public static class IsNumberRegistered extends RequestBase {
        private IsNumberRegistered() {
            inProcessRequestNumber = freeRequestCode;
            mustWaitForNewCompletion = false;
        }

        private Call call;

        public int sendRequest(final Activity activity, final String number, Completion completion) {
            if (inProcessRequestNumber != freeRequestCode) {
                return requestRejectionCode;
            }

            RequestBody formBody = new FormBody.Builder().add("number", number).build();
            Request request = new Request.Builder().url(serverBase + API_isNumberRegistered).post(formBody).build();
            call = client.newCall(request);

            final int currentRequestNumber = ++requestsNumber;
            this.completion = completion;
            mustWaitForNewCompletion = false;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final HashMap<String, Object> requestResult = new HashMap<>();
                    try {
                        final Response response = call.execute();
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    try {
                                        JSONObject reader = new JSONObject(response.body().string());
                                        requestResult.put(isRegisteredKey, reader.getBoolean("result"));
                                        requestResult.put(numberKey, number);
                                        requestResult.put(isSuccessKey, true);
                                    } catch (Exception e) {
                                        requestResult.put(isSuccessKey, false);
                                    } finally {
                                        response.body().close();
                                    }
                                } else {
                                    requestResult.put(isSuccessKey, false);
                                }

                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    } catch (IOException e) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                requestResult.put(isSuccessKey, false);
                                resultIsReady(currentRequestNumber, requestResult);
                            }
                        });
                    }
                }
            }).start();

            inProcessRequestNumber = currentRequestNumber;
            return currentRequestNumber;
        }

        public void killRequest(int requestCode) {
            if (requestCode != inProcessRequestNumber) {
                getUnprocessedResult(requestCode);
                return;
            }

            call.cancel();
            mustWaitForNewCompletion = false;
            inProcessRequestNumber = freeRequestCode;
        }
    }
    public static IsNumberRegistered isNumberRegistered = new IsNumberRegistered();
}
