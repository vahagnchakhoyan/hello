package Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.hello.R;

public final class AlertsManager {
    public interface AlertButtonClickCompletion {
        void execute();
    }

    private AlertsManager() {}

    public static void showAlert(Activity activity, CharSequence message, final AlertButtonClickCompletion completion) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        final AlertDialog dialog;

        if (Build.VERSION.SDK_INT >= 21) {
            LayoutInflater inflater = activity.getLayoutInflater();
            View view = inflater.inflate(R.layout.alert_simple, null);
            builder.setView(view);

            ((TextView) view.findViewById(R.id.messageTextView)).setText(message);
            dialog = builder.create();

            ((Button) view.findViewById(R.id.okButton)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    completion.execute();
                }
            });
        } else {
            builder.setTitle("Alert!");
            builder.setMessage(message);
            builder.setPositiveButton("Ok", null);
            dialog = builder.create();
        }

        dialog.show();
    }

    public static void showConfirmation(Activity activity, CharSequence message, final AlertButtonClickCompletion yesCompletion, final AlertButtonClickCompletion noCompletion) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        final AlertDialog dialog;
        
        if (Build.VERSION.SDK_INT >= 21) {
            LayoutInflater inflater = activity.getLayoutInflater();
            View view = inflater.inflate(R.layout.confirm_simple, null);
            builder.setView(view);

            ((TextView) view.findViewById(R.id.messageTextView)).setText(message);
            dialog = builder.create();

            ((Button) view.findViewById(R.id.yesButton)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    yesCompletion.execute();
                }
            });

            ((Button) view.findViewById(R.id.noButton)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    noCompletion.execute();
                }
            });
        } else {
            builder.setTitle("Confirmation!");
            builder.setMessage(message);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    yesCompletion.execute();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    noCompletion.execute();
                }
            });

            dialog = builder.create();
        }

        dialog.show();
    }
}
