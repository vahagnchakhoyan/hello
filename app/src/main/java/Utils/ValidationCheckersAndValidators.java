package Utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.example.hello.Properties;

public final class ValidationCheckersAndValidators {
    public static final int cameraPermissionRequestCode = 1;
    public static final int microphonePermissionRequestCode = 2;

    private ValidationCheckersAndValidators() {}

    public static boolean isValidPin(String testableString) {
        return testableString.matches("\\d{" + Properties.pinSymbolsCount + "}");
    }

    public static boolean isValidPinCandidate(String testableString) {
        return testableString.matches("\\d{0," + Properties.pinSymbolsCount + "}");
    }

    public static String getValidPinCandidateFromString(String prototype) {
        prototype = prototype.replaceAll("[^\\d]", "");
        prototype = prototype.substring(0, Math.min(prototype.length(), Properties.pinSymbolsCount));
        return prototype;
    }

    public static boolean isValidNumber(String testableString) {
        return testableString.matches("\\d{" + Properties.numberSymbolsCount + "}");
    }

    public static boolean isValidNumberCandidate(String testableString) {
        return testableString.matches("\\d{0," + Properties.numberSymbolsCount + "}");
    }

    public static String getValidNumberCandidateFromString(String prototype) {
        prototype = prototype.replaceAll("[^\\d]", "");
        prototype = prototype.substring(0, Math.min(prototype.length(), Properties.numberSymbolsCount));
        return prototype;
    }

    public static String getValidContactNameFromString(String name) {
        return name.trim().replaceAll("\\s{2,}", " ");
    }

    public static boolean haveCameraPermission(Context context) {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    public static boolean haveMicrophonePermission(Context context) {
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED);
    }

    public static void requestForCameraPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.CAMERA},
                cameraPermissionRequestCode);
    }

    public static void requestForMicrophonePermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.RECORD_AUDIO},
                microphonePermissionRequestCode);
    }
}
