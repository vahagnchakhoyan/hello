package Utils.NewPaletteTools;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

public class ScrollViewWithoutAutoscroll extends ScrollView {
    public ScrollViewWithoutAutoscroll(Context context) {
        super(context);
    }

    public ScrollViewWithoutAutoscroll(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollViewWithoutAutoscroll(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected boolean onRequestFocusInDescendants(int direction, Rect previouslyFocusedRect) {
        return false;
    }

    @Override
    public void requestChildFocus(View child, View focused) {
        int a = 4;
    }
}