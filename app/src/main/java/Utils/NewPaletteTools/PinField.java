package Utils.NewPaletteTools;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hello.R;

public class PinField {
    private TextView textView;
    private String text;
    private View view;

    public void attachViewToRoot(ViewGroup container) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        view = inflater.inflate(R.layout.pin_field, container, false);
        textView = view.findViewById(R.id.pinFieldTextView);
        if (text != null) textView.setText(text);
        container.addView(view);
    }

    public void setText(String digit) {
        text = digit;
        if (textView != null) textView.setText(digit);
    }

    public void removeViewFromParent() {
        ViewGroup parent = (ViewGroup) view.getParent();
        parent.removeView(view);
    }
}
