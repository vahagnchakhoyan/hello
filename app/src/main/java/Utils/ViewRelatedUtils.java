package Utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.hardware.Camera;
import android.support.constraint.ConstraintLayout;
import android.view.Surface;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ScrollView;

public final class ViewRelatedUtils {
    private ViewRelatedUtils() {}

    public static void setHeightPercentage(View viewToSet, View viewSetFrom, double percent) {
        ConstraintLayout.LayoutParams viewToSetParams = (ConstraintLayout.LayoutParams) viewToSet.getLayoutParams();
        viewToSetParams.height = (int) (viewSetFrom.getHeight() * percent);
        viewToSet.setLayoutParams(viewToSetParams);
    }

    public static void setWidthPercentage(View viewToSet, View viewSetFrom, double percent) {
        ConstraintLayout.LayoutParams viewToSetParams = (ConstraintLayout.LayoutParams) viewToSet.getLayoutParams();
        viewToSetParams.width = (int) (viewSetFrom.getWidth() * percent);
        viewToSet.setLayoutParams(viewToSetParams);
    }

    public static void setHeightAndWidthPercentage(View viewToSet, View viewSetFrom, double percent) {
        setHeightPercentage(viewToSet, viewSetFrom, percent);
        setWidthPercentage(viewToSet, viewSetFrom, percent);
    }

    public static void setHeight(View viewToSet, int newHeight) {
        ConstraintLayout.LayoutParams viewToSetParams = (ConstraintLayout.LayoutParams) viewToSet.getLayoutParams();
        viewToSetParams.height = newHeight;
        viewToSet.setLayoutParams(viewToSetParams);
    }

    public static void scroll(final ScrollView scrollView, final int direction) {
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(direction);
            }
        });
    }

    public static void closeKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void openKeyboard(Activity activity, View viewToOpenFor) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);

        inputMethodManager.showSoftInput(viewToOpenFor, InputMethodManager.SHOW_FORCED);
    }

    public static void closeKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isKeyboardOpen(Activity activity) {
        View decorView = activity.getWindow().getDecorView();
        Rect visibleRectangle = new Rect();

        decorView.getWindowVisibleDisplayFrame(visibleRectangle);

        int screenHeight = decorView.getHeight();
        int keyboardHeight = screenHeight - visibleRectangle.bottom;

        return keyboardHeight > screenHeight * 0.15;
    }

    public static void setCursorToTheEnd(EditText editText) {
        editText.setSelection(editText.getText().length());
    }

    public static void setCameraDisplayOrientation(Activity activity, int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);

        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }
}
