package Utils;

import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;

public final class EventsManipulators {
    private EventsManipulators() {}

    public static void setGlobalLayoutListener(View eventWaiter, ViewTreeObserver.OnGlobalLayoutListener listener) {
        eventWaiter.getViewTreeObserver().addOnGlobalLayoutListener(listener);
    }

    public static void setOnClickListener(View eventWaiter, View.OnClickListener listener) {
        eventWaiter.setOnClickListener(listener);
    }

    public static void removeGlobalLayoutListener(View eventWaiter, ViewTreeObserver.OnGlobalLayoutListener listener) {
        eventWaiter.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
    }

    public static void setTextChangeListener(EditText eventWaiter, TextWatcher listener) {
        eventWaiter.addTextChangedListener(listener);
    }

    public static void removeTextChangeListener(EditText eventWaiter, TextWatcher listener) {
        eventWaiter.removeTextChangedListener(listener);
    }
}
