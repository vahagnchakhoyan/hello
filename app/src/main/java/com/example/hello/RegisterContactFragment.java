package com.example.hello;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.HashMap;

import Managers.RequestsManager;
import Utils.EventsManipulators;
import Utils.ValidationCheckersAndValidators;
import Utils.ViewRelatedUtils;

import static Managers.ContactsManager.registerContact;
import static com.example.hello.Properties.SP_fileName;
import static com.example.hello.Properties.SP_numberKey;
import static com.example.hello.Properties.SP_tokenKey;

public class RegisterContactFragment extends Fragment {
    public static String numberArgument = "number";

    private SharedPreferences sharedPreferences;

    private static String isProcessingRegisterKey = "isProcessing";
    private static String registerContactRequestNumberKey = "requestNumber";

    private String number;

    private RegisterContactFragmentInteractionListener mListener;

    private ViewGroup fragmentView;
    private ProgressWheel processingView;
    private TextView numberTextView;
    private Button cancelButton;
    private EditText nameEditText;
    private Button registerButton;

    //on instance saves
    private boolean isProcessingRegister;
    private int registerContactRequestNumber;

    private boolean isSavedInstance;

    private View.OnClickListener cancelEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ViewRelatedUtils.closeKeyboard(getActivity());
            mListener.cancelClickedFromRegisterContact();
        }
    };

    private View.OnClickListener registerEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String name = ValidationCheckersAndValidators.getValidContactNameFromString(nameEditText.getText().toString());
            if(name.isEmpty()) {
                return;
            }

            int newRequestNumber = registerContact.sendRequest(getActivity(), sharedPreferences.getString(SP_numberKey, ""), sharedPreferences.getString(SP_tokenKey, ""), number, name, registerContactCompletion);
            if (newRequestNumber != RequestsManager.requestRejectionCode) {
                ViewRelatedUtils.closeKeyboard(getActivity());
                isProcessingRegister = true;
                registerContactRequestNumber = newRequestNumber;
                showProcessing();
            }
        }
    };

    private RequestsManager.Completion registerContactCompletion = new RequestsManager.Completion() {
        @Override
        public void execute(final HashMap<String, Object> result) {
            isProcessingRegister = false;
            hideProcessing();
            if (!(boolean) result.get(registerContact.isSuccessKey)) {
                return;
            }

            if((boolean) result.get(registerContact.isRegisterSuccessKey)) {
                mListener.contactRegistered();
            }
        }
    };

    public RegisterContactFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RegisterContactFragmentInteractionListener) {
            mListener = (RegisterContactFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RegisterContactFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        sharedPreferences = getActivity().getSharedPreferences(SP_fileName, Context.MODE_PRIVATE);

        number = getArguments().getString(numberArgument);

        if (savedInstance != null) {
            isProcessingRegister = savedInstance.getBoolean(isProcessingRegisterKey);
            registerContactRequestNumber = savedInstance.getInt(registerContactRequestNumberKey);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(fragmentView == null) {
            fragmentView = (ViewGroup) inflater.inflate(R.layout.fragment_register_contact, container, false);

            processingView = fragmentView.findViewById(R.id.registerContactProcessingWheel);
            cancelButton = fragmentView.findViewById(R.id.cancelButton);
            registerButton = fragmentView.findViewById(R.id.registerButton);
            numberTextView = fragmentView.findViewById(R.id.numberTextView);
            nameEditText = fragmentView.findViewById(R.id.nameEditText);

            numberTextView.setText(number);
        }

        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        isSavedInstance = false;

        setListeners();
        if(isProcessingRegister) showProcessing(); else hideProcessing();

        if(isProcessingRegister) {
            registerContact.applyNewCompletion(registerContactCompletion, registerContactRequestNumber);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        removeListeners();

        if (!isProcessingRegister) {
            return;
        }

        if (isSavedInstance) {
            registerContact.waitForNewCompletion(registerContactRequestNumber);
        } else {
            registerContact.killRequest(registerContactRequestNumber);
            isProcessingRegister = false;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(isProcessingRegisterKey, isProcessingRegister);
        outState.putInt(registerContactRequestNumberKey, registerContactRequestNumber);

        isSavedInstance = true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setListeners() {
        EventsManipulators.setOnClickListener(cancelButton, cancelEventListener);
        EventsManipulators.setOnClickListener(registerButton, registerEventListener);
    }

    private void removeListeners() {
        EventsManipulators.setOnClickListener(cancelButton, null);
        EventsManipulators.setOnClickListener(registerButton, null);
    }

    private void showProcessing() {
        processingView.setVisibility(View.VISIBLE);
        processingView.spin();
    }

    private void hideProcessing() {
        processingView.setVisibility(View.GONE);
        processingView.stopSpinning();
    }

    public interface RegisterContactFragmentInteractionListener {
        void cancelClickedFromRegisterContact();
        void contactRegistered();
    }
}
