package com.example.hello;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.HashMap;

import Managers.RequestsManager;
import Utils.EventsManipulators;
import Utils.NewPaletteTools.PinField;
import Utils.ValidationCheckersAndValidators;
import Utils.ViewRelatedUtils;

import static Managers.RequestsManager.confirmPin;
import static Managers.RequestsManager.numberAvailabilityCheck;
import static com.example.hello.Properties.SP_fileName;
import static com.example.hello.Properties.SP_numberKey;
import static com.example.hello.Properties.SP_tokenKey;

public class PinFragment extends Fragment {
    public static String numberArgument = "number";

    private static String isProcessingPinConfirmationKey = "isProcessing";
    private static String confirmRequestNumberKey = "requestNumber";

    private String number;
    private SharedPreferences sharedPreferences;

    private PinFragmentInteractionListener mListener;

    private ViewGroup fragmentView;
    private ScrollView scrollView;
    private ConstraintLayout wrapperView;
    private ConstraintLayout topView;
    private ConstraintLayout pinLayout;
    private Button backButton;
    private Button logInButton;
    private Button confirmButton;
    private ProgressWheel processingView;
    private TextView numberTextView;
    private LinearLayout pinFieldsLinearLayout;
    private EditText invisibleEditText;
    private PinField[] pinFields;

    private int fragmentViewHeight;
    private boolean performedInitialLayout;

    //on instance saves
    private boolean isProcessingPinConfirmation;
    private int confirmRequestNumber;

    private boolean isSavedInstance;

    private ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            if (performedInitialLayout) {
                checkIfFragmentViewHeightChangedThenHandle();
            } else if (ViewRelatedUtils.isKeyboardOpen(getActivity())) {
                //this will cause onGlobalLayout() call again
                ViewRelatedUtils.closeKeyboard(getActivity());
            } else {
                performInitialLayout();
                setListeners();
            }
        }
    };

    private View.OnClickListener backEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.backClicked();
        }
    };

    private View.OnClickListener logInEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.logInClickedFromPin();
        }
    };

    private View.OnClickListener pinFieldsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            invisibleEditText.requestFocus();
            ViewRelatedUtils.openKeyboard(getActivity(), invisibleEditText);
        }
    };

    private View.OnClickListener confirmEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String pin = invisibleEditText.getText().toString();
            if(ValidationCheckersAndValidators.isValidPin(pin)) {
                int newRequestNumber = RequestsManager.confirmPin.sendRequest(getActivity(), number, pin, confirmCompletion);
                if (newRequestNumber != RequestsManager.requestRejectionCode) {
                    ViewRelatedUtils.closeKeyboard(getActivity());
                    isProcessingPinConfirmation = true;
                    confirmRequestNumber = newRequestNumber;
                    showProcessing();
                }
            }
        }
    };

    private RequestsManager.Completion confirmCompletion = new RequestsManager.Completion() {
        @Override
        public void execute(HashMap<String, Object> result) {
            isProcessingPinConfirmation = false;
            hideProcessing();
            if (!(boolean) result.get(numberAvailabilityCheck.isSuccessKey)) {
                return;
            }

            if((boolean) result.get(confirmPin.confirmationSuccessKey)) {
                sharedPreferences.edit().putString(SP_tokenKey, (String) result.get(confirmPin.tokenKey)).putString(SP_numberKey, (String) result.get(confirmPin.numberKey)).apply();
                mListener.authenticated();
            }
        }
    };

    private TextWatcher pinWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void afterTextChanged(Editable s) {
            ViewRelatedUtils.setCursorToTheEnd(invisibleEditText);
            correctPinFieldsDigitsAndInvisibleEditText();
        }
    };

    public PinFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PinFragmentInteractionListener) {
            mListener = (PinFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement PinFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        number = getArguments().getString(numberArgument);
        sharedPreferences = getActivity().getSharedPreferences(SP_fileName, Context.MODE_PRIVATE);

        if (savedInstance != null) {
            isProcessingPinConfirmation = savedInstance.getBoolean(isProcessingPinConfirmationKey);
            confirmRequestNumber = savedInstance.getInt(confirmRequestNumberKey);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (fragmentView == null) {
            View view = inflater.inflate(R.layout.fragment_pin, container, false);
            fragmentView = (ViewGroup) view;
            performedInitialLayout = false;

            scrollView = view.findViewById(R.id.pinScrollView);
            wrapperView = view.findViewById(R.id.wrapperLayout);
            topView = view.findViewById(R.id.topLayout);
            pinLayout = view.findViewById(R.id.pinLayout);
            backButton = view.findViewById(R.id.backButton);
            logInButton = view.findViewById(R.id.logInButton);
            confirmButton = view.findViewById(R.id.confirmButton);
            processingView = view.findViewById(R.id.processWheel);
            numberTextView = view.findViewById(R.id.numberTextView);
            invisibleEditText = view.findViewById(R.id.invisibleEditText);
            pinFieldsLinearLayout = view.findViewById(R.id.pinFieldsLinearLayout);

            pinFields = new PinField[Properties.pinSymbolsCount];

            for (int i = 0; i < Properties.pinSymbolsCount; ++i) {
                pinFields[i] = new PinField();
                pinFields[i].attachViewToRoot(pinFieldsLinearLayout);
            }
        }

        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isProcessingPinConfirmation) {
            confirmPin.applyNewCompletion(confirmCompletion, confirmRequestNumber);
        }

        ViewRelatedUtils.closeKeyboard(getActivity());
        setListeners();
    }

    @Override
    public void onStop() {
        super.onStop();

        removeListeners();

        if (!isProcessingPinConfirmation) {
            return;
        }

        if (isSavedInstance) {
            confirmPin.waitForNewCompletion(confirmRequestNumber);
        } else {
            confirmPin.killRequest(confirmRequestNumber);
            isProcessingPinConfirmation = false;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(isProcessingPinConfirmationKey, isProcessingPinConfirmation);
        outState.putInt(confirmRequestNumberKey, confirmRequestNumber);

        isSavedInstance = true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setListeners() {
        EventsManipulators.setOnClickListener(backButton, backEventListener);
        EventsManipulators.setOnClickListener(logInButton, logInEventListener);
        EventsManipulators.setOnClickListener(confirmButton, confirmEventListener);
        EventsManipulators.setOnClickListener(pinFieldsLinearLayout, pinFieldsClickListener);
        EventsManipulators.setTextChangeListener(invisibleEditText, pinWatcher);
        EventsManipulators.setGlobalLayoutListener(fragmentView, globalLayoutListener);
    }

    private void removeListeners() {
        EventsManipulators.setOnClickListener(backButton, null);
        EventsManipulators.setOnClickListener(logInButton, null);
        EventsManipulators.setOnClickListener(confirmButton, null);
        EventsManipulators.setOnClickListener(pinFieldsLinearLayout, null);
        EventsManipulators.removeTextChangeListener(invisibleEditText, pinWatcher);
        EventsManipulators.removeGlobalLayoutListener(fragmentView, globalLayoutListener);
    }

    private void performInitialLayout() {
        fragmentViewHeight = fragmentView.getHeight();

        ViewRelatedUtils.setHeightPercentage(topView, scrollView, 0.3);
        ViewRelatedUtils.setHeightPercentage(pinLayout, scrollView, 0.4);
        numberTextView.setText(number);
        correctPinFieldsDigitsAndInvisibleEditText();

        if (isProcessingPinConfirmation) showProcessing(); else hideProcessing();

        wrapperView.requestFocus();

        performedInitialLayout = true;
    }

    private void correctPinFieldsDigitsAndInvisibleEditText() {
        String pin = invisibleEditText.getText().toString();
        if (ValidationCheckersAndValidators.isValidPinCandidate(pin)) {
            for (int i = 0; i < pin.length(); ++i) {
                pinFields[i].setText(pin.substring(i, i+1));
            }

            for (int i = pin.length(); i < Properties.pinSymbolsCount; ++i) {
                pinFields[i].setText("-");
            }
        } else {
            invisibleEditText.setText(ValidationCheckersAndValidators.getValidPinCandidateFromString(pin));
        }
    }

    private void showProcessing() {
        processingView.setVisibility(View.VISIBLE);
        processingView.spin();
    }

    private void hideProcessing() {
        processingView.setVisibility(View.GONE);
        processingView.stopSpinning();
    }

    private void checkIfFragmentViewHeightChangedThenHandle() {
        int newFragmentViewHeight = fragmentView.getHeight();

        if (newFragmentViewHeight != fragmentViewHeight) {
            fragmentViewHeight = newFragmentViewHeight;
            fragmentViewHeightChanged();
        }
    }

    private void fragmentViewHeightChanged() {
        ViewRelatedUtils.scroll(scrollView, View.FOCUS_DOWN);
    }

    public interface PinFragmentInteractionListener {
        void backClicked();
        void logInClickedFromPin();
        void authenticated();
    }
}
