package com.example.hello;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.pnikosis.materialishprogress.ProgressWheel;

import androidx.navigation.fragment.NavHostFragment;

import java.util.HashMap;

import Managers.RequestsManager;

import static Managers.RequestsManager.authenticateWithToken;
import static com.example.hello.Properties.SP_fileName;
import static com.example.hello.Properties.SP_numberKey;
import static com.example.hello.Properties.SP_tokenKey;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, MainFragment.MainFragmentInteractionListener, LogInFragment.LogInFragmentInteractionListener, PinFragment.PinFragmentInteractionListener {
    private static String isProcessingAuthenticationKey = "isProcessing";
    private static String authenticationRequestNumberKey = "requestNumber";

    private SharedPreferences sharedPreferences;

    private Fragment fragment;
    private ProgressWheel processingView;

    private boolean isProcessingAuthentication;
    private int authenticationRequestNumber;

    private RequestsManager.Completion authenticationCompletion = new RequestsManager.Completion() {
        @Override
        public void execute(HashMap<String, Object> result) {
            isProcessingAuthentication = false;
            hideProcessing();
            if(!(boolean) result.get(authenticateWithToken.isSuccessKey)) {
                return;
            }

            if((boolean) result.get(authenticateWithToken.authenticationSuccessKey)) {
                String number = (String) result.get(authenticateWithToken.numberKey);
                String token = (String) result.get(authenticateWithToken.tokenKey);

                sharedPreferences.edit().putString(SP_numberKey, number).putString(SP_tokenKey, token).apply();

                authenticated();
            } else {
                sharedPreferences.edit().remove(SP_tokenKey).remove(SP_numberKey).apply();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getSharedPreferences(SP_fileName, MODE_PRIVATE);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        setContentView(R.layout.activity_main);
        fragment = getSupportFragmentManager().findFragmentById(R.id.mainFragment);
        processingView = findViewById(R.id.mainActivityProcessWheel);

        if (savedInstanceState != null) {
            isProcessingAuthentication = savedInstanceState.getBoolean(isProcessingAuthenticationKey);
            authenticationRequestNumber = savedInstanceState.getInt(authenticationRequestNumberKey);
        }

        checkAuthenticationAndSetGraph();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(isProcessingAuthentication) {
            showProcessing();
            authenticateWithToken.applyNewCompletion(authenticationCompletion, authenticationRequestNumber);
        } else {
             hideProcessing();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (!isProcessingAuthentication) {
            return;
        }

        if (isFinishing()) {
            authenticateWithToken.killRequest(authenticationRequestNumber);
            isProcessingAuthentication = false;
        } else {
            authenticateWithToken.waitForNewCompletion(authenticationRequestNumber);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(isProcessingAuthenticationKey, isProcessingAuthentication);
        outState.putInt(authenticationRequestNumberKey, authenticationRequestNumber);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    @Override
    public void logInClickedFromMain() {
        cleanFragment();
        NavHostFragment.findNavController(fragment).navigate(R.id.action_mainFragment_to_logInFragment);
    }

    @Override
    public void logInClickedFromPin() {
        cleanFragment();
        NavHostFragment.findNavController(fragment).navigate(R.id.action_pinFragment_to_logInFragment);
    }

    @Override
    public void foundAvailableNumberInMain(String number) {
        Bundle bundle = new Bundle();
        bundle.putString(PinFragment.numberArgument, number);
        cleanFragment();
        NavHostFragment.findNavController(fragment).navigate(R.id.action_mainFragment_to_pinFragment, bundle);
    }

    @Override
    public void backClicked() {
        NavHostFragment.findNavController(fragment).navigateUp();
    }

    @Override
    public void authenticated() {
        Intent intent = new Intent(this, AuthenticatedActivity.class);
        startActivity(intent);
        finish();
    }

    private void checkAuthenticationAndSetGraph() {
        String token = sharedPreferences.getString(SP_tokenKey, null);
        String number = sharedPreferences.getString(SP_numberKey, null);

        if(token == null || number == null) {
            return;
        }

        int newRequestNumber = authenticateWithToken.sendRequest(this, token, number, authenticationCompletion);
        if(newRequestNumber == RequestsManager.requestRejectionCode) {
            return;
        }

        isProcessingAuthentication = true;
        authenticationRequestNumber = newRequestNumber;
        showProcessing();
    }

    private void showProcessing() {
        processingView.setVisibility(View.VISIBLE);
        processingView.spin();
    }

    private void hideProcessing() {
        processingView.setVisibility(View.GONE);
        processingView.stopSpinning();
    }

    private void cleanFragment() {
        ((ViewGroup) fragment.getView()).removeAllViews();
    }
}
