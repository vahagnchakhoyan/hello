package com.example.hello;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.HashMap;

import Managers.ContactsManager;
import Managers.RequestsManager;
import Utils.AlertsManager;
import Utils.EventsManipulators;
import Utils.ValidationCheckersAndValidators;
import Utils.ViewRelatedUtils;

import static Managers.ContactsManager.deleteContact;
import static Managers.ContactsManager.editContact;
import static com.example.hello.Properties.SP_fileName;
import static com.example.hello.Properties.SP_numberKey;
import static com.example.hello.Properties.SP_tokenKey;

public class ContactFragment extends Fragment {
    public static final String contactPositionArgument = "contactPosition";

    private SharedPreferences sharedPreferences;

    private static String isProcessingEditContactKey = "isProcessing";
    private static String editContactRequestNumberKey = "requestNumber";
    private static String isChangeNameLayoutVisibleKey = "isEditNameLayoutVisible";
    private static String changeNameLayoutTextKey = "changeNameLayoutText";
    private static String isDeleteConfirmationVisibleKey = "isDeleteConfirmationVisible";
    private static String isProcessingDeleteContactKey = "isProcessingDelete";
    private static String deleteContactRequestNumberKey = "deleteRequestNumber";

    private Contact contact;

    private contactFragmentInteractionListener mListener;

    private ViewGroup fragmentView;
    private ProgressWheel processingView;
    private Button backButton;
    private Button deleteContactButton;
    private ImageButton videocallButton;
    private ImageButton voicecallButton;
    private Button changeNameButton;
    private TextView contactNumberTextView;
    private TextView nameTextView;
    private EditText changeNameEditText;
    private Button cancelEditButton;
    private Button saveEditButton;
    private ConstraintLayout contactNameLayout;
    private ConstraintLayout editNameLayout;

    //on instance saves
    private boolean isProcessingEditContact;
    private int editContactRequestNumber;
    private boolean isEditNameLayoutVisible;
    private String editNameEditTextText;
    private boolean isDeleteConfirmationVisible;
    private boolean isProcessingDeleteContact;
    private int deleteContactRequestNumber;

    private boolean isSavedInstance;

    private View.OnClickListener backEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.backClicked();
        }
    };

    private View.OnClickListener deleteEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showDeleteConfirmation();
        }
    };

    private View.OnClickListener videocallEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.videocallClicked(contact);
        }
    };

    private View.OnClickListener voicecallEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.voicecallClicked(contact);
        }
    };

    private View.OnClickListener changeNameEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showEditNameLayout();
            changeNameEditText.setText(nameTextView.getText());
        }
    };

    private View.OnClickListener cancelEditEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hideEditNameLayout();
        }
    };

    private View.OnClickListener saveEditEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String newName = ValidationCheckersAndValidators.getValidContactNameFromString(changeNameEditText.getText().toString());
            if(newName.isEmpty()) {
                return;
            }

            Contact editedContact = new Contact(contact.getNumber(), newName);
            hideEditNameLayout();
            int newRequestNumber = editContact.sendRequest(getActivity(), sharedPreferences.getString(SP_numberKey, ""), sharedPreferences.getString(SP_tokenKey, ""), contact, editedContact, editContactCompletion);
            if (newRequestNumber != RequestsManager.requestRejectionCode) {
                isProcessingEditContact = true;
                editContactRequestNumber = newRequestNumber;
                showProcessing();
            }
        }
    };

    private AlertsManager.AlertButtonClickCompletion alertYesClickCompletion = new AlertsManager.AlertButtonClickCompletion() {
        @Override
        public void execute() {
            isDeleteConfirmationVisible = false;
            int newRequestNumber = deleteContact.sendRequest(getActivity(), sharedPreferences.getString(SP_numberKey, ""), sharedPreferences.getString(SP_tokenKey, ""), contact, deleteContactCompletion);
            if (newRequestNumber != RequestsManager.requestRejectionCode) {
                isProcessingDeleteContact = true;
                deleteContactRequestNumber = newRequestNumber;
                showProcessing();
            }
        }
    };

    private AlertsManager.AlertButtonClickCompletion alertNoClickCompletion = new AlertsManager.AlertButtonClickCompletion() {
        @Override
        public void execute() {
            isDeleteConfirmationVisible = false;
        }
    };

    private RequestsManager.Completion deleteContactCompletion = new RequestsManager.Completion() {
        @Override
        public void execute(final HashMap<String, Object> result) {
            isProcessingDeleteContact = false;
            hideProcessing();
            if (!(boolean) result.get(deleteContact.isSuccessKey)) {
                return;
            }

            if((boolean) result.get(deleteContact.deleteContactSuccessKey)) {
                mListener.deletedContactFromContact(contact);
            }
        }
    };

    private RequestsManager.Completion editContactCompletion = new RequestsManager.Completion() {
        @Override
        public void execute(final HashMap<String, Object> result) {
            isProcessingEditContact = false;
            hideProcessing();
            if (!(boolean) result.get(editContact.isSuccessKey)) {
                return;
            }

            if((boolean) result.get(editContact.editContactSuccessKey)) {
                Contact editedContact = (Contact) result.get(editContact.editedContactKey);
                //should become function set appearance according contact
                nameTextView.setText(editedContact.getName());
            }
        }
    };

    public ContactFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof contactFragmentInteractionListener) {
            mListener = (contactFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement contactFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        sharedPreferences = getActivity().getSharedPreferences(SP_fileName, Context.MODE_PRIVATE);

        int contactPosition = getArguments().getInt(contactPositionArgument);
        contact = ContactsManager.getContactByPosition(contactPosition);

        if (savedInstance != null) {
            isProcessingEditContact = savedInstance.getBoolean(isProcessingEditContactKey);
            editContactRequestNumber = savedInstance.getInt(editContactRequestNumberKey);
            isEditNameLayoutVisible = savedInstance.getBoolean(isChangeNameLayoutVisibleKey);
            editNameEditTextText = savedInstance.getString(changeNameLayoutTextKey);
            isDeleteConfirmationVisible = savedInstance.getBoolean(isDeleteConfirmationVisibleKey);
            isProcessingDeleteContact = savedInstance.getBoolean(isProcessingDeleteContactKey);
            deleteContactRequestNumber = savedInstance.getInt(deleteContactRequestNumberKey);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (fragmentView == null) {
            View view = inflater.inflate(R.layout.fragment_contact, container, false);
            fragmentView = (ViewGroup) view;

            processingView = view.findViewById(R.id.contactProcessingWheel);
            backButton = view.findViewById(R.id.backButton);
            deleteContactButton = view.findViewById(R.id.deleteContactButton);
            videocallButton = view.findViewById(R.id.videocallButton);
            voicecallButton = view.findViewById(R.id.voicecallButton);
            changeNameButton = view.findViewById(R.id.changeNameButton);
            contactNumberTextView = view.findViewById(R.id.contactNumberTextView);
            nameTextView = view.findViewById(R.id.contactNameTextView);
            changeNameEditText = view.findViewById(R.id.contactNameEditText);
            cancelEditButton = view.findViewById(R.id.cancelButton);
            saveEditButton = view.findViewById(R.id.saveButton);
            contactNameLayout = view.findViewById(R.id.contactNameLayout);
            editNameLayout = view.findViewById(R.id.editNameLayout);

            nameTextView.setText(contact.getName());
            contactNumberTextView.setText(contact.getNumber());
            if(isEditNameLayoutVisible) {showEditNameLayout(); changeNameEditText.setText(editNameEditTextText);} else hideEditNameLayout();
        }

        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isProcessingDeleteContact || isProcessingEditContact) showProcessing(); else hideProcessing();
        if(isProcessingDeleteContact) {
            deleteContact.applyNewCompletion(deleteContactCompletion, deleteContactRequestNumber);
        }

        if(isProcessingEditContact) {
            editContact.applyNewCompletion(editContactCompletion, editContactRequestNumber);
        }

        if(isDeleteConfirmationVisible) showDeleteConfirmation();

        setListeners();
    }

    @Override
    public void onStop() {
        super.onStop();

        removeListeners();

        if (!isProcessingEditContact && !isProcessingDeleteContact) {
            return;
        }

        if (isSavedInstance) {
            if(isProcessingEditContact) editContact.waitForNewCompletion(editContactRequestNumber);
            if(isProcessingDeleteContact) deleteContact.waitForNewCompletion(deleteContactRequestNumber);
        } else {
            if(isProcessingEditContact) {editContact.killRequest(editContactRequestNumber); isProcessingEditContact = false;}
            if(isProcessingDeleteContact) {deleteContact.killRequest(deleteContactRequestNumber); isProcessingDeleteContact = false;}
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(isProcessingEditContactKey, isProcessingEditContact);
        outState.putInt(editContactRequestNumberKey, editContactRequestNumber);
        outState.putBoolean(isChangeNameLayoutVisibleKey, isEditNameLayoutVisible);
        outState.putString(changeNameLayoutTextKey, editNameEditTextText);
        outState.putBoolean(isDeleteConfirmationVisibleKey, isDeleteConfirmationVisible);
        outState.putBoolean(isProcessingDeleteContactKey, isProcessingDeleteContact);
        outState.putInt(deleteContactRequestNumberKey, deleteContactRequestNumber);

        isSavedInstance = true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setListeners() {
        EventsManipulators.setOnClickListener(backButton, backEventListener);
        EventsManipulators.setOnClickListener(deleteContactButton, deleteEventListener);
        EventsManipulators.setOnClickListener(videocallButton, videocallEventListener);
        EventsManipulators.setOnClickListener(voicecallButton, voicecallEventListener);
        EventsManipulators.setOnClickListener(changeNameButton, changeNameEventListener);
        EventsManipulators.setOnClickListener(cancelEditButton, cancelEditEventListener);
        EventsManipulators.setOnClickListener(saveEditButton, saveEditEventListener);
    }

    private void removeListeners() {
        EventsManipulators.setOnClickListener(backButton, null);
        EventsManipulators.setOnClickListener(deleteContactButton, null);
        EventsManipulators.setOnClickListener(videocallButton, null);
        EventsManipulators.setOnClickListener(voicecallButton, null);
        EventsManipulators.setOnClickListener(changeNameButton, null);
        EventsManipulators.setOnClickListener(cancelEditButton, null);
        EventsManipulators.setOnClickListener(saveEditButton, null);
    }

    private void showProcessing() {
        processingView.setVisibility(View.VISIBLE);
        processingView.spin();
    }

    private void hideProcessing() {
        processingView.setVisibility(View.GONE);
        processingView.stopSpinning();
    }

    private void hideEditNameLayout() {
        ViewRelatedUtils.closeKeyboard(getActivity());
        isEditNameLayoutVisible = false;
        contactNameLayout.setVisibility(View.VISIBLE);
        editNameLayout.setVisibility(View.GONE);
    }

    private void showEditNameLayout() {
        isEditNameLayoutVisible = true;
        contactNameLayout.setVisibility(View.GONE);
        editNameLayout.setVisibility(View.VISIBLE);
    }

    private void showDeleteConfirmation() {
        isDeleteConfirmationVisible = true;
        AlertsManager.showConfirmation(getActivity(), "Are you sure want to delete this contact?", alertYesClickCompletion, alertNoClickCompletion);
    }

    public interface contactFragmentInteractionListener {
        void backClicked();
        void videocallClicked(Contact contact);
        void voicecallClicked(Contact contact);
        void deletedContactFromContact(Contact contact);
    }
}
