package com.example.hello;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;

import androidx.navigation.fragment.NavHostFragment;

import Managers.ContactsManager;

import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;

import static Managers.RequestsManager.logOut;
import static com.example.hello.Properties.SP_fileName;
import static com.example.hello.Properties.SP_numberKey;
import static com.example.hello.Properties.SP_tokenKey;

public class AuthenticatedActivity extends AppCompatActivity implements ContactsFragment.ContactsFragmentInteractionListener, SettingsFragment.SettingsFragmentInteractionListener, ContactFragment.contactFragmentInteractionListener, AddContactFragment.AddContactFragmentInteractionListener, RegisterContactFragment.RegisterContactFragmentInteractionListener {
    private SharedPreferences sharedPreferences;

    private Fragment fragment;
    private Pusher pusher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getSharedPreferences(SP_fileName, MODE_PRIVATE);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar

        setContentView(R.layout.activity_authenticated);

        fragment = getSupportFragmentManager().findFragmentById(R.id.authenticatedfragment);

        PusherOptions options = new PusherOptions();
        options.setCluster("mt1");
        pusher = new Pusher("e561f126649854dbd6d3", options);

        Channel channel = pusher.subscribe("test-channel");

        channel.bind("test-event", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                int a = 5;
            }
        });

        pusher.connect();
    }

    @Override
    public void settingsClickedFromContacts() {
        cleanFragment();
        NavHostFragment.findNavController(fragment).navigate(R.id.action_contactsFragment_to_settingsFragment);
    }

    @Override
    public void addContactClickedFromContacts() {
        cleanFragment();
        NavHostFragment.findNavController(fragment).navigate(R.id.action_contactsFragment_to_addContactFragment);
    }

    @Override
    public void contactClicked(int contactPosition) {
        cleanFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ContactFragment.contactPositionArgument, contactPosition);
        NavHostFragment.findNavController(fragment).navigate(R.id.action_contactsFragment_to_contactFragment, bundle);
    }

    @Override
    public void logOutClicked() {
        logOut.sendRequest(this, sharedPreferences.getString(SP_numberKey, ""), sharedPreferences.getString(SP_tokenKey, ""));
        sharedPreferences.edit().remove(SP_tokenKey).remove(SP_numberKey).apply();
        ContactsManager.loggingOut();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void backClicked() {
        NavHostFragment.findNavController(fragment).navigateUp();
    }

    @Override
    public void videocallClicked(Contact contact) {
        int index = ContactsManager.getIndexOfContact(contact);
        Intent intent = new Intent(this, ActiveCallService.class);
        intent.putExtra(ActiveCallService.contactIndexKey, index);
        intent.putExtra(ActiveCallService.isVideocallKey, true);
        startService(intent);
    }

    @Override
    public void voicecallClicked(Contact contact) {
        int index = ContactsManager.getIndexOfContact(contact);
        Intent intent = new Intent(this, ActiveCallService.class);
        intent.putExtra(ActiveCallService.contactIndexKey, index);
        intent.putExtra(ActiveCallService.isVideocallKey, false);
        startService(intent);
    }

    @Override
    public void deletedContactFromContact(Contact contact) {
        NavHostFragment.findNavController(fragment).navigateUp();
    }

    @Override
    public void foundRegisteredNumber(String number) {
        Bundle bundle = new Bundle();
        bundle.putString(RegisterContactFragment.numberArgument, number);
        cleanFragment();
        NavHostFragment.findNavController(fragment).navigate(R.id.action_addContactFragment_to_registerContactFragment, bundle);
    }

    @Override
    public void cancelClickedFromRegisterContact() {
        NavHostFragment.findNavController(fragment).navigateUp();
    }

    @Override
    public void contactRegistered() {
        NavHostFragment.findNavController(fragment).navigateUp();
    }

    private void cleanFragment() {
        ((ViewGroup) fragment.getView()).removeAllViews();
    }
}
