package com.example.hello;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import Utils.EventsManipulators;

import static com.example.hello.Properties.SP_fileName;
import static com.example.hello.Properties.SP_numberKey;

public class SettingsFragment extends Fragment {
    private SharedPreferences sharedPreferences;

    private SettingsFragmentInteractionListener mListener;

    private ViewGroup fragmentView;
    private TextView numberTextView;
    private Button backButton;
    private Button logOutButton;

    private View.OnClickListener backEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.backClicked();
        }
    };

    private View.OnClickListener logOutEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.logOutClicked();
        }
    };

    public SettingsFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SettingsFragmentInteractionListener) {
            mListener = (SettingsFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement SettingsFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        sharedPreferences = getActivity().getSharedPreferences(SP_fileName, Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(fragmentView == null) {
            fragmentView = (ViewGroup) inflater.inflate(R.layout.fragment_settings, container, false);
            numberTextView = fragmentView.findViewById(R.id.numberTextView);

            backButton = fragmentView.findViewById(R.id.backButton);
            logOutButton = fragmentView.findViewById(R.id.logOutButton);

            numberTextView.setText(sharedPreferences.getString(SP_numberKey, "NO NUMBER"));
        }

        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();

        setListeners();
    }

    @Override
    public void onStop() {
        super.onStop();

        removeListeners();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setListeners() {
        EventsManipulators.setOnClickListener(backButton, backEventListener);
        EventsManipulators.setOnClickListener(logOutButton, logOutEventListener);
    }

    private void removeListeners() {
        EventsManipulators.setOnClickListener(backButton, null);
        EventsManipulators.setOnClickListener(logOutButton, null);
    }

    public interface SettingsFragmentInteractionListener {
        void backClicked();
        void logOutClicked();
    }
}
