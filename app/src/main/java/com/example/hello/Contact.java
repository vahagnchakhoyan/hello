package com.example.hello;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Contact {
    private String number;
    private String name;

    public Contact(String number, String name) {
        this.number = number;
        this.name = name;
    }

    public Contact(JSONObject jsonContact) throws JSONException {
       this.number = jsonContact.getString("contact_number");
       this.name = jsonContact.getString("contact_name");
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public void setFromAntother(Contact anotherContact) {
        number = anotherContact.getNumber();
        name = anotherContact.getName();
    }

//    public HashMap<String, Object> toDictionary() {
//        HashMap<String, Object> dictionary = new HashMap<>();
//        dictionary.put("number", number);
//        dictionary.put("name", name);
//        return dictionary;
//    }
}
