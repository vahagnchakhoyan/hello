package com.example.hello;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;

import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.HashMap;

import Managers.RequestsManager;
import Utils.AlertsManager;
import Utils.EventsManipulators;
import Utils.ValidationCheckersAndValidators;
import Utils.ViewRelatedUtils;

import static Managers.RequestsManager.confirmPin;
import static Managers.RequestsManager.logIn;
import static com.example.hello.Properties.SP_fileName;
import static com.example.hello.Properties.SP_numberKey;
import static com.example.hello.Properties.SP_tokenKey;

public class LogInFragment extends Fragment {
    private static String isProcessingLogInKey = "isProcessing";
    private static String logInRequestNumberKey = "requestNumber";
    private static String isAlertVisibleKey = "isAlertVisible";

    private LogInFragmentInteractionListener mListener;

    private SharedPreferences sharedPreferences;

    private ViewGroup fragmentView;
    private ScrollView scrollView;
    private ConstraintLayout wrapperView;
    private ConstraintLayout topView;
    private ConstraintLayout logInLayout;
    private Button backButton;
    private EditText numberField;
    private EditText pinField;
    private Button logInButton;
    private ProgressWheel processingView;

    private int fragmentViewHeight;
    private boolean performedInitialLayout;

    //on instance saves
    private boolean isProcessingLogIn;
    private int logInRequestNumber;
    private boolean isAlertVisible;

    private boolean isSavedInstance;

    private ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            if (performedInitialLayout) {
                checkIfFragmentViewHeightChangedThenHandle();
            } else if (ViewRelatedUtils.isKeyboardOpen(getActivity())) {
                //this will cause onGlobalLayout() call again
                ViewRelatedUtils.closeKeyboard(getActivity());
            } else {
                performInitialLayout();
            }
        }
    };

    private View.OnClickListener backEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.backClicked();
        }
    };

    private View.OnClickListener logInEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!ValidationCheckersAndValidators.isValidNumber(numberField.getText().toString()) || !ValidationCheckersAndValidators.isValidPin(pinField.getText().toString())) {
                AlertsManager.showAlert(getActivity(), "Please input valid number and pin.", alertOkClickCompletion);
                isAlertVisible = true;
                return;
            }

            int newRequestNumber = RequestsManager.logIn.sendRequest(getActivity(), numberField.getText().toString(), pinField.getText().toString(), logInCompletion);

            if (newRequestNumber != RequestsManager.requestRejectionCode) {
                ViewRelatedUtils.closeKeyboard(getActivity());
                isProcessingLogIn = true;
                logInRequestNumber = newRequestNumber;
                showProcessing();
            }
        }
    };

    private AlertsManager.AlertButtonClickCompletion alertOkClickCompletion = new AlertsManager.AlertButtonClickCompletion() {
        @Override
        public void execute() {
            isAlertVisible = false;
        }
    };

    private RequestsManager.Completion logInCompletion = new RequestsManager.Completion() {
        @Override
        public void execute(final HashMap<String, Object> result) {
            isProcessingLogIn = false;
            hideProcessing();
            if (!(boolean) result.get(logIn.isSuccessKey)) {
                return;
            }

            if((boolean) result.get(logIn.logInSuccessKey)) {
                sharedPreferences.edit().putString(SP_tokenKey, (String) result.get(confirmPin.tokenKey)).putString(SP_numberKey, (String) result.get(confirmPin.numberKey)).apply();
                mListener.authenticated();
            }
        }
    };

    public LogInFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LogInFragmentInteractionListener) {
            mListener = (LogInFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement PinFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        sharedPreferences = getActivity().getSharedPreferences(SP_fileName, Context.MODE_PRIVATE);

        if (savedInstance != null) {
            isProcessingLogIn = savedInstance.getBoolean(isProcessingLogInKey);
            logInRequestNumber = savedInstance.getInt(logInRequestNumberKey);
            isAlertVisible = savedInstance.getBoolean(isAlertVisibleKey);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (fragmentView == null) {
            View view = inflater.inflate(R.layout.fragment_log_in, container, false);
            fragmentView = (ViewGroup) view;
            performedInitialLayout = false;

            scrollView = view.findViewById(R.id.logInScrollView);
            wrapperView = view.findViewById(R.id.wrapperLayout);
            topView = view.findViewById(R.id.topLayout);
            logInLayout = view.findViewById(R.id.logInLayout);
            backButton = view.findViewById(R.id.backButton);
            numberField = view.findViewById(R.id.numberField);
            pinField = view.findViewById(R.id.passwordField);
            logInButton = view.findViewById(R.id.logInButton);
            processingView = view.findViewById(R.id.processWheel);
        }

        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        isSavedInstance = false;

        if (isProcessingLogIn) {
            logIn.applyNewCompletion(logInCompletion, logInRequestNumber);
        }

        ViewRelatedUtils.closeKeyboard(getActivity());
        setListeners();
    }

    @Override
    public void onStop() {
        super.onStop();

        removeListeners();

        if (!isProcessingLogIn) {
            return;
        }

        if (isSavedInstance) {
            logIn.waitForNewCompletion(logInRequestNumber);
        } else {
            logIn.killRequest(logInRequestNumber);
            isProcessingLogIn = false;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(isProcessingLogInKey, isProcessingLogIn);
        outState.putInt(logInRequestNumberKey, logInRequestNumber);
        outState.putBoolean(isAlertVisibleKey, isAlertVisible);

        isSavedInstance = true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setListeners() {
        EventsManipulators.setOnClickListener(backButton, backEventListener);
        EventsManipulators.setOnClickListener(logInButton, logInEventListener);
        EventsManipulators.setGlobalLayoutListener(fragmentView, globalLayoutListener);
    }

    private void removeListeners() {
        EventsManipulators.setOnClickListener(backButton, null);
        EventsManipulators.setOnClickListener(logInButton, null);
        EventsManipulators.removeGlobalLayoutListener(fragmentView, globalLayoutListener);
    }

    private void performInitialLayout() {
        fragmentViewHeight = fragmentView.getHeight();

        ViewRelatedUtils.setHeightPercentage(topView, scrollView, 0.3);
        ViewRelatedUtils.setHeightPercentage(logInLayout, scrollView, 0.4);

        if (isProcessingLogIn) showProcessing(); else hideProcessing();
        if (isAlertVisible) AlertsManager.showAlert(getActivity(), "Please input valid number and pin.", alertOkClickCompletion);

        wrapperView.requestFocus();

        performedInitialLayout = true;
    }

    private void showProcessing() {
        processingView.setVisibility(View.VISIBLE);
        processingView.spin();
    }

    private void hideProcessing() {
        processingView.setVisibility(View.GONE);
        processingView.stopSpinning();
    }

    private void checkIfFragmentViewHeightChangedThenHandle() {
        int newFragmentViewHeight = fragmentView.getHeight();

        if (newFragmentViewHeight != fragmentViewHeight) {
            fragmentViewHeight = newFragmentViewHeight;
            fragmentViewHeightChanged();
        }
    }

    private void fragmentViewHeightChanged() {
        ViewRelatedUtils.scroll(scrollView, View.FOCUS_DOWN);
    }

    public interface LogInFragmentInteractionListener {
        void backClicked();
        void authenticated();
    }
}
