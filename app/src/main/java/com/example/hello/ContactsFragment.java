package com.example.hello;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.HashMap;

import Adapters.ContactsListViewAdapter;
import Managers.RequestsManager;
import Utils.EventsManipulators;

import static Managers.ContactsManager.getContacts;
import static com.example.hello.Properties.SP_fileName;
import static com.example.hello.Properties.SP_numberKey;
import static com.example.hello.Properties.SP_tokenKey;

public class ContactsFragment extends Fragment {
    private static String isProcessingGetContactsKey = "isProcessing";
    private static String getContactsRequestNumberKey = "requestNumber";

    private SharedPreferences sharedPreferences;

    private ContactsFragmentInteractionListener mListener;

    private ViewGroup fragmentView;
    private ListView contactsListView;
    private ProgressWheel processingView;
    private TextView numberTextView;
    private Button settingsButton;
    private Button addContactButton;

    //on instance saves
    private boolean isProcessingGetContacts;
    private int getContactsRequestNumber;
    private boolean isNewlyOpenedPage;

    private boolean isSavedInstance;

    private View.OnClickListener settingsEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.settingsClickedFromContacts();
        }
    };

    private View.OnClickListener addContactEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.addContactClickedFromContacts();
        }
    };

    private RequestsManager.Completion getContactsCompletion = new RequestsManager.Completion() {
        @Override
        public void execute(final HashMap<String, Object> result) {
            isProcessingGetContacts = false;
            hideProcessing();
            if (!(boolean) result.get(getContacts.isSuccessKey)) {
                return;
            }

            ArrayList<Contact> contacts = (ArrayList<Contact>) result.get(getContacts.contactsKey);
            contactsListView.setAdapter(new ContactsListViewAdapter(getContext(), contacts));
            contactsListView.invalidate();
        }
    };

    private AdapterView.OnItemClickListener contactClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.appMainColor));
            mListener.contactClicked(position);
        }
    };

    public ContactsFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ContactsFragmentInteractionListener) {
            mListener = (ContactsFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement ContactsFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        sharedPreferences = getActivity().getSharedPreferences(SP_fileName, Context.MODE_PRIVATE);

        if (savedInstance != null) {
            isProcessingGetContacts = savedInstance.getBoolean(isProcessingGetContactsKey);
            getContactsRequestNumber = savedInstance.getInt(getContactsRequestNumberKey);
            isNewlyOpenedPage = false;
        } else {
            isNewlyOpenedPage = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(fragmentView == null) {
            fragmentView = (ViewGroup) inflater.inflate(R.layout.fragment_contacts, container, false);
            numberTextView = fragmentView.findViewById(R.id.numberTextView);
            contactsListView = fragmentView.findViewById(R.id.contactsListView);

            processingView = fragmentView.findViewById(R.id.contactsProcessingWheel);
            settingsButton = fragmentView.findViewById(R.id.settingsButton);
            addContactButton = fragmentView.findViewById(R.id.addContactButton);

            contactsListView.setOnItemClickListener(contactClickListener);
            numberTextView.setText(sharedPreferences.getString(SP_numberKey, "NO NUMBER"));
        }

        if(!isProcessingGetContacts) sendInitialGetContactsRequest();

        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        isSavedInstance = false;

        setListeners();
        if (isProcessingGetContacts) showProcessing(); else hideProcessing();

        if (isNewlyOpenedPage) {
            isNewlyOpenedPage = false;
        } else if (isProcessingGetContacts) {
            getContacts.applyNewCompletion(getContactsCompletion, getContactsRequestNumber);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        removeListeners();

        if (!isProcessingGetContacts) {
            return;
        }

        if (isSavedInstance) {
            getContacts.waitForNewCompletion(getContactsRequestNumber);
        } else {
            getContacts.killRequest(getContactsRequestNumber);
            isProcessingGetContacts = false;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(isProcessingGetContactsKey, isProcessingGetContacts);
        outState.putInt(getContactsRequestNumberKey, getContactsRequestNumber);

        isSavedInstance = true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setListeners() {
        EventsManipulators.setOnClickListener(settingsButton, settingsEventListener);
        EventsManipulators.setOnClickListener(addContactButton, addContactEventListener);
    }

    private void removeListeners() {
        EventsManipulators.setOnClickListener(settingsButton, null);
        EventsManipulators.setOnClickListener(addContactButton, null);
    }

    private void showProcessing() {
        processingView.setVisibility(View.VISIBLE);
        processingView.spin();
    }

    private void hideProcessing() {
        processingView.setVisibility(View.GONE);
        processingView.stopSpinning();
    }

    private void sendInitialGetContactsRequest() {
        int newRequestNumber = getContacts.sendRequest(getActivity(), sharedPreferences.getString(SP_numberKey, ""), sharedPreferences.getString(SP_tokenKey, ""), getContactsCompletion);
        if (newRequestNumber != RequestsManager.requestRejectionCode && newRequestNumber != RequestsManager.noNeedToSendRequestCode) {
            isProcessingGetContacts = true;
            getContactsRequestNumber = newRequestNumber;
            showProcessing();
        }
    }

    public interface ContactsFragmentInteractionListener {
        void settingsClickedFromContacts();
        void addContactClickedFromContacts();
        void contactClicked(int contactPosition);
    }
}
