package com.example.hello;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;

import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.HashMap;

import Managers.RequestsManager;
import Utils.AlertsManager;
import Utils.EventsManipulators;
import Utils.ValidationCheckersAndValidators;
import Utils.ViewRelatedUtils;

import static Managers.RequestsManager.numberAvailabilityCheck;

public class MainFragment extends Fragment {
    private static String isProcessingCheckNumberKey = "isProcessing";
    private static String checkRequestNumberKey = "requestNumber";
    private static String isAlertVisibleKey = "isAlertVisible";

    private MainFragmentInteractionListener mListener;

    private ViewGroup fragmentView;
    private ScrollView scrollView;
    private ConstraintLayout wrapperView;
    private ConstraintLayout topView;
    private ConstraintLayout checkNumberLayout;
    private Button logInButton;
    private EditText numberField;
    private Button checkButton;
    private ProgressWheel processingView;

    private int fragmentViewHeight;
    private boolean performedInitialLayout;

    //on instance saves
    private boolean isProcessingCheckNumber;
    private int checkRequestNumber;
    private boolean isAlertVisible;

    private boolean isSavedInstance;

    private ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            if (performedInitialLayout) {
                checkIfFragmentViewHeightChangedThenHandle();
            } else if (ViewRelatedUtils.isKeyboardOpen(getActivity())) {
                //this will cause onGlobalLayout() call again
                ViewRelatedUtils.closeKeyboard(getActivity());
            } else {
                performInitialLayout();
            }
        }
    };

    private View.OnClickListener logInEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.logInClickedFromMain();
        }
    };

    private AlertsManager.AlertButtonClickCompletion alertOkClickCompletion = new AlertsManager.AlertButtonClickCompletion() {
        @Override
        public void execute() {
            isAlertVisible = false;
        }
    };

    private View.OnClickListener checkEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!ValidationCheckersAndValidators.isValidNumber(numberField.getText().toString())) {
                AlertsManager.showAlert(getActivity(), "Valid number must contain " + Properties.numberSymbolsCount + " digits.", alertOkClickCompletion);
                isAlertVisible = true;
                return;
            }

            int newRequestNumber = RequestsManager.numberAvailabilityCheck.sendRequest(getActivity(), numberField.getText().toString(), numberAvailabilityCheckCompletion);

            if (newRequestNumber != RequestsManager.requestRejectionCode) {
                ViewRelatedUtils.closeKeyboard(getActivity());
                isProcessingCheckNumber = true;
                checkRequestNumber = newRequestNumber;
                showProcessing();
            }
        }
    };

    private RequestsManager.Completion numberAvailabilityCheckCompletion = new RequestsManager.Completion() {
        @Override
        public void execute(final HashMap<String, Object> result) {
            isProcessingCheckNumber = false;
            hideProcessing();
            if (!(boolean) result.get(numberAvailabilityCheck.isSuccessKey)) {
                return;
            }

            if((boolean) result.get(numberAvailabilityCheck.checkResultKey)) {
                mListener.foundAvailableNumberInMain((String) result.get(numberAvailabilityCheck.numberKey));
            }
        }
    };

    public MainFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainFragmentInteractionListener) {
            mListener = (MainFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement PinFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        if (savedInstance != null) {
            isProcessingCheckNumber = savedInstance.getBoolean(isProcessingCheckNumberKey);
            checkRequestNumber = savedInstance.getInt(checkRequestNumberKey);
            isAlertVisible = savedInstance.getBoolean(isAlertVisibleKey);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (fragmentView == null) {
            View view = inflater.inflate(R.layout.fragment_main, container, false);
            fragmentView = (ViewGroup) view;
            performedInitialLayout = false;

            scrollView = view.findViewById(R.id.registerScrollView);
            wrapperView = view.findViewById(R.id.wrapperLayout);
            topView = view.findViewById(R.id.topLayout);
            checkNumberLayout = view.findViewById(R.id.checkNumberLayout);
            logInButton = view.findViewById(R.id.logInButton);
            numberField = view.findViewById(R.id.numberField);
            checkButton = view.findViewById(R.id.checkNumberButton);
            processingView = view.findViewById(R.id.processWheel);
        }

        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        isSavedInstance = false;

        if (isProcessingCheckNumber) {
            numberAvailabilityCheck.applyNewCompletion(numberAvailabilityCheckCompletion, checkRequestNumber);
        }

        ViewRelatedUtils.closeKeyboard(getActivity());
        setListeners();
    }

    @Override
    public void onStop() {
        super.onStop();

        removeListeners();

        if (!isProcessingCheckNumber) {
            return;
        }

        if (isSavedInstance) {
            numberAvailabilityCheck.waitForNewCompletion(checkRequestNumber);
        } else {
            numberAvailabilityCheck.killRequest(checkRequestNumber);
            isProcessingCheckNumber = false;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(isProcessingCheckNumberKey, isProcessingCheckNumber);
        outState.putInt(checkRequestNumberKey, checkRequestNumber);
        outState.putBoolean(isAlertVisibleKey, isAlertVisible);

        isSavedInstance = true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setListeners() {
        EventsManipulators.setOnClickListener(logInButton, logInEventListener);
        EventsManipulators.setOnClickListener(checkButton, checkEventListener);
        EventsManipulators.setGlobalLayoutListener(fragmentView, globalLayoutListener);
    }

    private void removeListeners() {
        EventsManipulators.setOnClickListener(logInButton, null);
        EventsManipulators.setOnClickListener(checkButton, null);
        EventsManipulators.removeGlobalLayoutListener(fragmentView, globalLayoutListener);
    }

    private void performInitialLayout() {
        fragmentViewHeight = fragmentView.getHeight();

        ViewRelatedUtils.setHeightPercentage(topView, scrollView, 0.3);
        ViewRelatedUtils.setHeightPercentage(checkNumberLayout, scrollView, 0.4);

        if (isProcessingCheckNumber) showProcessing(); else hideProcessing();
        if (isAlertVisible) AlertsManager.showAlert(getActivity(), "Valid number must contain " + Properties.numberSymbolsCount + " digits.", alertOkClickCompletion);

        wrapperView.requestFocus();

        performedInitialLayout = true;
    }

    private void showProcessing() {
        processingView.setVisibility(View.VISIBLE);
        processingView.spin();
    }

    private void hideProcessing() {
        processingView.setVisibility(View.GONE);
        processingView.stopSpinning();
    }

   private void checkIfFragmentViewHeightChangedThenHandle() {
        int newFragmentViewHeight = fragmentView.getHeight();

        if (newFragmentViewHeight != fragmentViewHeight) {
            fragmentViewHeight = newFragmentViewHeight;
            fragmentViewHeightChanged();
        }
    }

    private void fragmentViewHeightChanged() {
        ViewRelatedUtils.scroll(scrollView, View.FOCUS_DOWN);
    }

    public interface MainFragmentInteractionListener {
        void logInClickedFromMain();
        void foundAvailableNumberInMain(String number);
    }
}
