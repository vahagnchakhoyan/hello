package com.example.hello;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.HashMap;

import Managers.ContactsManager;
import Managers.RequestsManager;
import Utils.EventsManipulators;
import Utils.ValidationCheckersAndValidators;
import Utils.ViewRelatedUtils;

import static Managers.RequestsManager.isNumberRegistered;

public class AddContactFragment extends Fragment {
    private static String isProcessingIsNumberRegisteredKey = "isProcessing";
    private static String isNumberRegisteredRequestNumberKey = "requestNumber";

    private AddContactFragmentInteractionListener mListener;

    private ViewGroup fragmentView;
    private ProgressWheel processingView;
    private Button backButton;
    private EditText numberEditText;
    private Button addButton;

    //on instance saves
    private boolean isProcessingIsNumberRegistered;
    private int isNumberRegisteredRequestNumber;

    private boolean isSavedInstance;

    private View.OnClickListener backEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ViewRelatedUtils.closeKeyboard(getActivity());
            mListener.backClicked();
        }
    };

    private View.OnClickListener addEventListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String number = numberEditText.getText().toString();
            if(!ValidationCheckersAndValidators.isValidNumber(number) && ContactsManager.getContactByNumber(number) != null) {
                return;
            }

            int newRequestNumber = isNumberRegistered.sendRequest(getActivity(), number, isNumberRegisteredCompletion);
            if (newRequestNumber != RequestsManager.requestRejectionCode) {
                ViewRelatedUtils.closeKeyboard(getActivity());
                isProcessingIsNumberRegistered = true;
                isNumberRegisteredRequestNumber = newRequestNumber;
                showProcessing();
            }
        }
    };

    private RequestsManager.Completion isNumberRegisteredCompletion = new RequestsManager.Completion() {
        @Override
        public void execute(final HashMap<String, Object> result) {
            isProcessingIsNumberRegistered = false;
            hideProcessing();
            if (!(boolean) result.get(isNumberRegistered.isSuccessKey)) {
                return;
            }

            if((boolean) result.get(isNumberRegistered.isRegisteredKey)) {
                String number = (String) result.get(isNumberRegistered.numberKey);
                mListener.foundRegisteredNumber(number);
            }
        }
    };

    public AddContactFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AddContactFragmentInteractionListener) {
            mListener = (AddContactFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement AddContactFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        if (savedInstance != null) {
            isProcessingIsNumberRegistered = savedInstance.getBoolean(isProcessingIsNumberRegisteredKey);
            isNumberRegisteredRequestNumber = savedInstance.getInt(isNumberRegisteredRequestNumberKey);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(fragmentView == null) {
            fragmentView = (ViewGroup) inflater.inflate(R.layout.fragment_add_contact, container, false);

            processingView = fragmentView.findViewById(R.id.addContactProcessingWheel);
            backButton = fragmentView.findViewById(R.id.backButton);
            numberEditText = fragmentView.findViewById(R.id.numberEditText);
            addButton = fragmentView.findViewById(R.id.addButton);
        }

        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        isSavedInstance = false;

        setListeners();
        if(isProcessingIsNumberRegistered) showProcessing(); else hideProcessing();

        if(isProcessingIsNumberRegistered) {
            isNumberRegistered.applyNewCompletion(isNumberRegisteredCompletion, isNumberRegisteredRequestNumber);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        removeListeners();

        if (!isProcessingIsNumberRegistered) {
            return;
        }

        if (isSavedInstance) {
            isNumberRegistered.waitForNewCompletion(isNumberRegisteredRequestNumber);
        } else {
            isNumberRegistered.killRequest(isNumberRegisteredRequestNumber);
            isProcessingIsNumberRegistered = false;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(isProcessingIsNumberRegisteredKey, isProcessingIsNumberRegistered);
        outState.putInt(isNumberRegisteredRequestNumberKey, isNumberRegisteredRequestNumber);

        isSavedInstance = true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setListeners() {
        EventsManipulators.setOnClickListener(backButton, backEventListener);
        EventsManipulators.setOnClickListener(addButton, addEventListener);
    }

    private void removeListeners() {
        EventsManipulators.setOnClickListener(backButton, null);
        EventsManipulators.setOnClickListener(addButton, null);
    }

    private void showProcessing() {
        processingView.setVisibility(View.VISIBLE);
        processingView.spin();
    }

    private void hideProcessing() {
        processingView.setVisibility(View.GONE);
        processingView.stopSpinning();
    }

    public interface AddContactFragmentInteractionListener {
        void backClicked();
        void foundRegisteredNumber(String number);
    }
}
