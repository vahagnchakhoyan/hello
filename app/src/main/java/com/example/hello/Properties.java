package com.example.hello;

public final class Properties {
    private Properties() {}

    public static final String SP_fileName = "Hello.shared.preferences";
    public static final String SP_tokenKey = "token";
    public static final String SP_numberKey = "number";

    public static String serverBase = "http://192.168.0.105:80";
    public static String socketServerBase = "192.168.0.105";
    public static int socketServerPort = 6001;
    public static int pinSymbolsCount = 4;
    public static int numberSymbolsCount = 8;

    public static final String API_authenticateWithToken = "/api/authenticateWithToken";
    public static final String API_confirmPin = "/api/confirmPin";
    public static final String API_deleteContact = "/api/deleteContact";
    public static final String API_editContact = "/api/editContact";
    public static final String API_getContacts = "/api/getContacts";
    public static final String API_isNumberRegistered = "/api/isNumberRegistered";
    public static final String API_logIn = "/api/logIn";
    public static final String API_logOut = "/api/logOut";
    public static final String API_numberAvailabilityCheck = "/api/numberAvailabilityCheck";
    public static final String API_registerContact = "/api/registerContact";
}
