package com.example.hello;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import Managers.ContactsManager;

public class ActiveCallService extends Service {
    public static final String contactIndexKey = "contactIndex";
    public static final String isVideocallKey = "isVideocall";

    private static final String activeCallNotificationChannelId = "ACTIVE_CALL_NOTIFICATION_CHANNEL_ID";

    private static ActiveCallService instance;
    public static ActiveCallService getInstance() {
        return instance;
    }

    private Camera camera;
    public Camera getCamera() {
        camera = getCameraInstance(onUseCameraNumber);
        return camera;
    }

    private boolean isNewCreatedActiveCall;

    private Contact contact;
    public String getContactName() {
        return contact.getName();
    }
    public String getContactNumber() {
        return contact.getNumber();
    }

    private boolean isMyVideoEnabled;
    public boolean isMyVideoEnabled() {
        return isMyVideoEnabled;
    }

    private boolean isPartnerVideoEnabled;
    public boolean isPartnerVideEnabled() {
        return isPartnerVideoEnabled;
    }

    private boolean isMicrophoneEnabled;
    public boolean isMicrophoneEnabled() {
        return isMicrophoneEnabled;
    }

    private int onUseCameraNumber;
    public int onUseCameraNumber() {
        return onUseCameraNumber;
    }

    private boolean isAnswered;
    public boolean isAnswered() {
        return isAnswered;
    }

    private boolean hasCamera;
    public boolean haveCamera() {
        return hasCamera;
    }

    private int numberOfCameras;
    public int numberOfCameras() {
        return numberOfCameras;
    }

    public ActiveCallService() {}

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        isNewCreatedActiveCall = true;
        instance = this;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(isNewCreatedActiveCall) {
            contact = ContactsManager.getContactByPosition(intent.getIntExtra(ActiveCallService.contactIndexKey, -1));
            isMyVideoEnabled = intent.getBooleanExtra(ActiveCallService.isVideocallKey, false);

            setInitialValues();
            getHardwareResources();

            setUpUserNotification();
            startActivity(new Intent(this, ActiveCallActivity.class));

            isNewCreatedActiveCall = false;
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if(camera != null) camera.release();
    }

    private void setUpUserNotification() {
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, ActiveCallActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, activeCallNotificationChannelId)
                .setContentTitle("Hello")
                .setContentText("Active Call with " + contact.getName())
                .setSmallIcon(R.drawable.voicecall_icon)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);
    }

    private void setInitialValues() {
        isMicrophoneEnabled = true;
        isAnswered = false;
        hasCamera = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
        numberOfCameras = Camera.getNumberOfCameras();
        onUseCameraNumber = hasCamera ? 0 : -1;
        isPartnerVideoEnabled = false;
    }

    private void getHardwareResources() {
        if(hasCamera) {
            camera = getCameraInstance(onUseCameraNumber);
        }
    }

    public Camera getCameraInstance(int cameraIndex){
        Camera camera = null;
        try {
            camera = Camera.open(cameraIndex); // attempt to get a Camera instance
        } catch (Exception e){ /* Camera is not available (in use or does not exist)*/ }
        return camera; // returns null if camera is unavailable
    }

    public boolean turnOffPressed() {
        stopSelf();
        return true;
    }

    public boolean rotateCameraPressed() {
        onUseCameraNumber = (onUseCameraNumber + 1) % numberOfCameras;
        if(camera != null) camera.release();
        camera = getCameraInstance(onUseCameraNumber);
        camera.lock();
        return true;
    }

    public boolean enableDisableCameraPressed() {
        isMyVideoEnabled = !isMyVideoEnabled;
        return true;
    }

    public boolean enableDisableMicrophonePressed() {
        isMicrophoneEnabled = !isMicrophoneEnabled;
        return true;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    activeCallNotificationChannelId,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
}
