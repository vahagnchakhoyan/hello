package com.example.hello;

import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import java.util.ArrayList;

import Utils.NewPaletteTools.CamerasView;
import Utils.ValidationCheckersAndValidators;
import Utils.ViewRelatedUtils;

import static Utils.ValidationCheckersAndValidators.cameraPermissionRequestCode;
import static Utils.ValidationCheckersAndValidators.microphonePermissionRequestCode;

public class ActiveCallActivity extends AppCompatActivity {
    private ActiveCallService activeCallService;
    private static final String cameraPermissionKey = "cameraPermission";
    private static final String microphonePermissionkey = "microphonePermission";

    private CamerasView selfCameraView;
    private ArrayList<String> permissionsQueue = new ArrayList<String>();

    public void turnOffEventListener(View view) {
        if(activeCallService.turnOffPressed()) {
            finish();
        }
    }

    public void cameraRotateEventListener(View view) {
        if(activeCallService.rotateCameraPressed()) {
            setupSelfCameraView();
        }
    }

    public void enableDisableCameraEventListener(View view) {
        if(!ValidationCheckersAndValidators.haveCameraPermission(this)) {
            ValidationCheckersAndValidators.requestForCameraPermission(this);
            return;
        }

        if(activeCallService.enableDisableCameraPressed()) {
            setCameraEnabledDisabled();
        }
    }

    public void enableDisableMicrophoneEventListener(View view) {
        if(!ValidationCheckersAndValidators.haveMicrophonePermission(this)) {
            ValidationCheckersAndValidators.requestForMicrophonePermission(this);
            return;
        }

        if(activeCallService.enableDisableMicrophonePressed()) {
            performHaveMicrophonePermissionLayout();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar

        setContentView(R.layout.activity_active_call);

        activeCallService = ActiveCallService.getInstance();

        performLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();

        setupSelfCameraView();
        passThroughPermissionsQueue();
    }

    private void passThroughPermissionsQueue() {
        if(!permissionsQueue.isEmpty()) {
            String permissionKey = permissionsQueue.remove(0);
            switch (permissionKey) {
                case cameraPermissionKey: {
                    ValidationCheckersAndValidators.requestForCameraPermission(this);
                    return;
                }
                case microphonePermissionkey: {
                    ValidationCheckersAndValidators.requestForMicrophonePermission(this);
                    return;
                }
            }
        }
    }

    private void performLayout() {
        performPartnerDataLayout();
        if(ValidationCheckersAndValidators.haveCameraPermission(this)) performHaveCameraPermissionLayout(); else performHaveNoCameraPermissionLayout();
        if(ValidationCheckersAndValidators.haveMicrophonePermission(this)) performHaveMicrophonePermissionLayout(); else performHaveNoMicrophonePermissionLayout();
    }

    private void performPartnerDataLayout() {
        ((TextView) findViewById(R.id.partnerNameTextView)).setText(activeCallService.getContactName());
        ((TextView) findViewById(R.id.partnerNumberTextView)).setText(activeCallService.getContactNumber());
    }

    private void performHaveNoCameraPermissionLayout() {
        findViewById(R.id.enableDisableSelfCameraButton).setBackgroundResource(R.drawable.no_camera_icon);
        findViewById(R.id.rotateCameraButton).setEnabled(false);
        hideSelfVideoView();

        permissionsQueue.add(cameraPermissionKey);
    }

    private void performHaveCameraPermissionLayout() {
        if(activeCallService.haveCamera()) performHaveCameraLayout(); else performHaveNoCameraLayout();
    }

    private void performHaveNoMicrophonePermissionLayout() {
        findViewById(R.id.enableDisableMicrophoneButton).setBackgroundResource(R.drawable.no_microphone_icon);

        permissionsQueue.add(microphonePermissionkey);
    }

    private void performHaveMicrophonePermissionLayout() {
        findViewById(R.id.enableDisableMicrophoneButton).setBackgroundResource(activeCallService.isMicrophoneEnabled() ? R.drawable.microphone_icon : R.drawable.no_microphone_icon);
    }

    private void performHaveCameraLayout() {
        setCameraEnabledDisabled();
        findViewById(R.id.enableDisableSelfCameraButton).setEnabled(true);
    }

    private void performHaveNoCameraLayout() {
        findViewById(R.id.enableDisableSelfCameraButton).setBackgroundResource(R.drawable.no_camera_icon);
        findViewById(R.id.enableDisableSelfCameraButton).setEnabled(false);
        findViewById(R.id.rotateCameraButton).setEnabled(false);
        hideSelfVideoView();
    }

    private void setupSelfCameraView() {
        Camera camera = activeCallService.getCamera();
        ((ConstraintLayout) findViewById(R.id.selfVideoView)).removeAllViews();
        if(camera != null) {
            ViewRelatedUtils.setCameraDisplayOrientation(this, activeCallService.onUseCameraNumber(), camera);
            selfCameraView = new CamerasView(this, camera);
            ((ConstraintLayout) findViewById(R.id.selfVideoView)).addView(selfCameraView);
        } else {
            selfCameraView = null;
            //Toast.makeText(this, "The camera is not available.", Toast.LENGTH_SHORT).show();
        }
    }

    private void hideSelfVideoView() {
        findViewById(R.id.selfVideoView).setVisibility(View.INVISIBLE);
    }

    private void showSelfVideoView() {
        findViewById(R.id.selfVideoView).setVisibility(View.VISIBLE);
    }

    private void setCameraEnabledDisabled() {
        findViewById(R.id.enableDisableSelfCameraButton).setBackgroundResource(activeCallService.isMyVideoEnabled() ? R.drawable.camera_icon : R.drawable.no_camera_icon);
        if(activeCallService.isMyVideoEnabled()) showSelfVideoView(); else hideSelfVideoView();
        findViewById(R.id.rotateCameraButton).setEnabled(activeCallService.numberOfCameras() > 1 && activeCallService.isMyVideoEnabled());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch(requestCode) {
            case cameraPermissionRequestCode: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    performHaveCameraPermissionLayout();
                }
                break;
            }
            case microphonePermissionRequestCode: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    performHaveMicrophonePermissionLayout();
                }
                break;
            }
        }
        passThroughPermissionsQueue();
    }
}
